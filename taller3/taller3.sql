--1.Liste el promedio de salario de todos los empleados
SELECT AVG(S.PAYMENT) promedio
FROM S_SALARY S;

--2. Liste el maximo salario de todos los empleados

SELECT MAX(S.PAYMENT) maximoSalario
FROM S_SALARY S;

--3. Liste el numero de empleados que ganan comision

SELECT COUNT(E.ID) empleados_con_comision
FROM S_EMP E WHERE COMMISSION_PCT IS NOT NULL ;

--4. Liste el numero total de empleados de la compañia

SELECT COUNT(E.ID) cantidad_empleados
FROM S_EMP E;


--5. Liste el numero de item por orden
SELECT O.ID orden,COUNT(I.ITEM_ID) num_item
FROM S_ITEM I,S_ORD O
WHERE I.ORD_ID = O.ID 
GROUP BY O.ID;

--6. Liste el numero de clientes por cada uno de los presentantes de ventas

SELECT E.FIRST_NAME ||' '|| E.LAST_NAME empleado,COUNT(C.ID) num_clientes
FROM S_EMP E,S_CUSTOMER C
WHERE C.SALES_REP_ID = E.ID 
GROUP BY E.FIRST_NAME ||' '|| E.LAST_NAME;

--7. Liste el numero de departamentos por region

SELECT R.NAME region,COUNT(D.ID) num_departamentos 
FROM S_DEPT D,S_REGION R
WHERE R.ID = D.REGION_ID 
GROUP BY R.NAME ;

--8. Liste el salario (sum(salario)+sum(ventas)*(min(comision)/100)) de cada representante de ventas

SELECT E.FIRST_NAME ||' '|| E.LAST_NAME empleado,(sum(S.PAYMENT)+sum(O.TOTAL)*(min(COMMISSION_PCT)/100))
FROM S_EMP E,S_SALARY S,S_ORD O
WHERE O.SALES_REP_ID = E.ID 
AND E.ID = S.ID 
GROUP BY E.FIRST_NAME ||' '|| E.LAST_NAME ;

--9. Liste el promedio de salario por nombre de cargo
SELECT E.TITLE,AVG(S.PAYMENT) 
FROM S_SALARY S,S_EMP E
WHERE S.ID =E.ID 
GROUP BY E.TITLE ;


--10. Liste por producto las veces que ha sido ordenado
SELECT P.NAME, COUNT(I.ORD_ID) veces_Ordenado
FROM S_ORD O,S_ITEM I,S_PRODUCT P
WHERE O.ID = I.ORD_ID 
AND I.PRODUCT_ID = P.ID 
GROUP BY P.NAME ;

--11.Liste el promedio de salario, el maximo, el total del salario y el numero
-- de empleados de cada uno de los nombres de departamentos de la compañia

SELECT D.NAME, AVG(S.PAYMENT)salario_promedio, MAX(S.PAYMENT) salario_max, sum(S.PAYMENT) total_salario, COUNT(DISTINCT E.ID) num_empleados  
FROM S_DEPT D,S_EMP E,S_SALARY S
WHERE E.DEPT_ID = D.ID
AND E.ID = S.ID
GROUP BY D.NAME;

--12. Listar los empleados que trabajan en el mismo departamento de Ngao
SELECT C.FIRST_NAME || ' ' || C.LAST_NAME compañeros
FROM S_EMP E,S_EMP C
WHERE  E.LAST_NAME = 'Ngao'
AND E.DEPT_ID = C.DEPT_ID ;

--13. Liste el promedio de salario total por nombre departamento y nombre de cargo
SELECT D.NAME ,E.TITLE ,avg(S.PAYMENT)salario_prom
FROM S_DEPT D,S_EMP E,S_SALARY S
WHERE D.ID = E.DEPT_ID 
AND E.ID = S.ID 
GROUP BY D.NAME , E.TITLE ;

--14. Liste el promedio de salario total por nombre de cargo y nombre de departamento
SELECT E.TITLE  ,D.NAME ,avg(S.PAYMENT)salario_prom
FROM S_DEPT D,S_EMP E,S_SALARY S
WHERE D.ID = E.DEPT_ID 
AND E.ID = S.ID 
GROUP BY D.NAME , E.TITLE ;

--15.Liste el promedio anual del salario y numero de trabajadores de todos los cargos
--(nombres) que tienen mas de 2 empleados
SELECT E.TITLE,AVG(S.PAYMENT )promedio_anual, count(DISTINCT E.ID) num_trabajadores
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID
HAVING count( DISTINCT E.ID)>2
GROUP BY E.TITLE  ;

--16. Liste la suma de salarios anuales y el numero de empleados de todos los departamentos
--(nombres) con mas de un empleado

SELECT D.NAME DEPARTAMENTO,sum(S.PAYMENT) salarios_anuales, COUNT(DISTINCT  E.ID) num_empleados
FROM S_EMP E,S_SALARY S,S_DEPT D
WHERE E.DEPT_ID = D.ID 
AND E.ID = S.ID
HAVING COUNT(DISTINCT E.ID) >1
GROUP BY D.NAME;


--17. Liste por cargo(nombres) el total de nomina anual superior a 5000. Con etiquetas

SELECT COUNT(DISTINCT E.ID),SUM(S.PAYMENT) 
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID 
HAVING SUM(S.PAYMENT)>5000
GROUP BY E.TITLE ;


--18. Liste los productos que han sido ordenados mas de dos veces (muestre el numero de veces)

SELECT I.PRODUCT_ID id_producto,P.NAME producto ,COUNT(*) num_ordenes
FROM S_ORD O JOIN S_ITEM I ON O.ID = I.ORD_ID JOIN S_PRODUCT P ON I.PRODUCT_ID = P.ID  
GROUP BY I.PRODUCT_ID,P.NAME
HAVING COUNT(*) > 2
ORDER BY I.PRODUCT_ID;

--19. Liste el numero de clientes por region

SELECT R.NAME,COUNT(*) num_clientes
FROM S_CUSTOMER C,S_REGION R
WHERE C.REGION_ID = R.ID 
GROUP BY R.NAME  ;

--20. Por region y producto ordenado liste los clientes que han hecho mas de dos ordenes
SELECT R.NAME region ,COUNT(C.ID) num_ord_cliente 
FROM S_REGION R,S_CUSTOMER C,S_ORD O
WHERE R.ID = C.REGION_ID 
AND C.ID = O.CUSTOMER_ID 
HAVING COUNT(C.ID)>2
GROUP BY R.NAME;

--21. Liste los empleados cuyo promedio anual de salario supera los 3000
SELECT E.ID ,E.FIRST_NAME || ' '||E.LAST_NAME empleado,AVG(S.PAYMENT) PROMEDIO_ANUAL
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID
HAVING AVG(S.PAYMENT)>3000 
GROUP BY E.ID,E.FIRST_NAME || ' '||E.LAST_NAME;

--22. Por orden liste el total del valor, el promedio del valor y el numero de productos
--expedidos en el 2011
SELECT O.ID, SUM(I.PRICE) total_valor, AVG(I.PRICE) promedio_valor, SUM(I.QUANTITY) expedidos
FROM S_ORD O,S_ITEM I
WHERE O.ID = I.ORD_ID 
AND TO_CHAR(O.DATE_SHIPPED,'YYYY') = '2011' 
GROUP BY O.ID ;

