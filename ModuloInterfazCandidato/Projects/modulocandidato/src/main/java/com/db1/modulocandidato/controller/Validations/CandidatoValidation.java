package com.db1.modulocandidato.controller.Validations;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.AssertFalse;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CandidatoValidation {
    @Email(message = "Ingrese un correo valido")
    @NotBlank( message = "Complete los campos")
    String usuario;
    @NotBlank( message = "Complete los campos")
    String nombre;
    @NotBlank( message = "Complete los campos")
    String apellido;
    @NotNull(message = "Ingrese una fecha")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    LocalDate fechaNac;
    @Positive(message = "Digite un documento valido")
    @NotNull
    Number nDoc;
    @NotBlank
    String tipoDoc;

    @AssertFalse(
        message = "La longitud de los campos debe ser menor a 30"
    )
    private boolean longCampos(){
        return usuario.length()>30 && nombre.length()>30 && apellido.length()>30;
    }
}
