package com.db1.modulocandidato.repository;

import java.util.List;

import com.db1.modulocandidato.models.TipoDoc;

public interface TipoDocRepository {
    public List<TipoDoc> getAll();
}
