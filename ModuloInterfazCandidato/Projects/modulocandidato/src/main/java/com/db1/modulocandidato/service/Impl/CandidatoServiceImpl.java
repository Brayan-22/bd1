package com.db1.modulocandidato.service.Impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db1.modulocandidato.models.Candidato;
import com.db1.modulocandidato.repository.CandidatoRepository;
import com.db1.modulocandidato.service.CandidatoService;

@Service
public class CandidatoServiceImpl implements CandidatoService{

    private final CandidatoRepository candidatoRepository;
    private final Logger logger = LoggerFactory.getLogger(CandidatoServiceImpl.class);
    
    public CandidatoServiceImpl(CandidatoRepository candidatoRepository) {
        this.candidatoRepository = candidatoRepository;
    }


    @Override
    public int saveCandidato(Candidato candidato) {
        return candidatoRepository.saveCandidato(candidato);
    }
    
}
