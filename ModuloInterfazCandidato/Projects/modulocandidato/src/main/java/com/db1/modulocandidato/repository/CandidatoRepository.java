package com.db1.modulocandidato.repository;

import java.util.List;

import com.db1.modulocandidato.models.Candidato;

public interface CandidatoRepository {
    public List<Candidato> getAll();
    public int saveCandidato(Candidato candidato);
    public void borrar(Candidato candidato);
}
