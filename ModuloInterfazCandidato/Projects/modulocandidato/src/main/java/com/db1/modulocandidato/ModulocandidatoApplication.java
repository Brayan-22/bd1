package com.db1.modulocandidato;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModulocandidatoApplication {
	public static void main(String[] args) {
		SpringApplication.run(ModulocandidatoApplication.class, args);
	}
}
