package com.db1.modulocandidato.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.db1.modulocandidato.controller.Validations.CandidatoValidation;
import com.db1.modulocandidato.models.Candidato;
import com.db1.modulocandidato.models.TipoDoc;
import com.db1.modulocandidato.service.CandidatoService;
import com.db1.modulocandidato.service.TipoDocService;


@Controller
public class RegistroController {
    private Logger logger = LoggerFactory.getLogger(RegistroController.class);
    private CandidatoService candidatoService;
    private TipoDocService tipoDocService;
    private List<TipoDoc> options;
    public RegistroController(@Autowired CandidatoService candidatoService,TipoDocService tipoDocService) {
        this.candidatoService = candidatoService;
        this.tipoDocService = tipoDocService;
        this.options = tipoDocService.getAll();
    }
    
    @GetMapping("/")
    public String form(@RequestParam(required = false) String success,Model model) {
        if (success!=null) {
            if (success.equals("Exito")) {
                model.addAttribute("success", "Candidato registrado correctamente");
            }
        }
        model.addAttribute("options", options);
        CandidatoValidation candidatoValidation = new CandidatoValidation();
        model.addAttribute("candidato", candidatoValidation);
        return "formRegistro";
    }

    @PostMapping("/")
    public String registro(Model model,@Validated@ModelAttribute("candidato") CandidatoValidation candidato,BindingResult result) {
        if (result.hasErrors()) {
            model.addAttribute("options", options);
            return "formRegistro";
        }
        Integer success = candidatoService.saveCandidato(Candidato.builder()
                                                              .usuario(candidato.getUsuario())
                                                              .nombre(candidato.getNombre())
                                                              .apellido(candidato.getApellido())
                                                              .nDoc(candidato.getNDoc())
                                                              .fechaNac(candidato.getFechaNac())
                                                              .tipoDoc(candidato.getTipoDoc()).build());
        if (success !=1) {
            model.addAttribute("options", options);
            model.addAttribute("error", "El usuario del candidato ingresado ya existe");
            return "formRegistro";
        }
        return "redirect:/?success=Exito";
    }
    

    
    
}
