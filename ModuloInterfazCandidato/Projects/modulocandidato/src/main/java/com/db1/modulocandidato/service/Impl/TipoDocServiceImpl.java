package com.db1.modulocandidato.service.Impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.db1.modulocandidato.models.TipoDoc;
import com.db1.modulocandidato.repository.TipoDocRepository;
import com.db1.modulocandidato.service.TipoDocService;

@Service
public class TipoDocServiceImpl implements TipoDocService{

    private final Logger logger = LoggerFactory.getLogger(TipoDocServiceImpl.class);
    private TipoDocRepository tipoDocRepository;    

    public TipoDocServiceImpl(TipoDocRepository tipoDocRepository) {
        this.tipoDocRepository = tipoDocRepository;
    }


    @Override
    public List<TipoDoc> getAll() {
        return tipoDocRepository.getAll();
    }
    
}
