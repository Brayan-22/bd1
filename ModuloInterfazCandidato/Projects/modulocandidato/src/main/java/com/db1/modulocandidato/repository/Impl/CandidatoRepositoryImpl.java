package com.db1.modulocandidato.repository.Impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.db1.modulocandidato.models.Candidato;
import com.db1.modulocandidato.models.TipoDoc;
import com.db1.modulocandidato.repository.CandidatoRepository;

@Repository
public class CandidatoRepositoryImpl implements CandidatoRepository{
    
    private static final Logger logger = LoggerFactory.getLogger(CandidatoRepositoryImpl.class);    
    private JdbcTemplate plantilla;

    
    public CandidatoRepositoryImpl(JdbcTemplate plantilla) {
        this.plantilla = plantilla;
    }

    private final RowMapper<Candidato> rowMapper = (rs,rowNum) ->{
        Candidato c = new Candidato();
        c.setUsuario(rs.getString("USUARIO"));
        c.setNombre(rs.getString("NOMBRE"));
        c.setApellido(rs.getString("APELLIDO"));
        c.setFechaNac(rs.getDate("FECHANAC").toLocalDate());
        c.setNDoc(rs.getLong("NDOC"));
        c.setTipoDoc(rs.getString("IDTIPODOC"));
        return c;
    };
    @Override
    public List<Candidato> getAll() {
        String sql ="select usuario,nombre,apellido,fechanac,ndoc,t.idtipodoc,desctipodoc from candidato c,tipodoc t where c.IDTIPODOC=t.IDTIPODOC";
        // String sql = "select * from CANDIDATO";
        return plantilla.query(sql, rowMapper);
    }

    @Override
    public int saveCandidato(Candidato candidato) {
        String sql = "insert into CANDIDATO (USUARIO,IDTIPODOC,NOMBRE,APELLIDO,FECHANAC,NDOC) values (?,?,?,?,?,?)";
        int success = 0;
        try {
            plantilla.update(sql, 
        candidato.getUsuario(),
        candidato.getTipoDoc(),
        candidato.getNombre(),
        candidato.getApellido(),
        candidato.getFechaNac(),
        candidato.getNDoc());
        success = 1;
        } catch (Exception e) {
        }
        return success;
    }

    @Override
    public void borrar(Candidato candidato) {
    }

    

}
