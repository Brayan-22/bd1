package com.db1.modulocandidato.service;

import com.db1.modulocandidato.models.Candidato;

public interface CandidatoService {
    public int saveCandidato(Candidato candidato);
}
