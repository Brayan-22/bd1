package com.db1.modulocandidato.models;

import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString(includeFieldNames = false)
public class Candidato {
    String usuario;
    String nombre;
    String apellido;
    LocalDate fechaNac;
    Number nDoc;
    String tipoDoc;
    // String tipoDoc;
}
