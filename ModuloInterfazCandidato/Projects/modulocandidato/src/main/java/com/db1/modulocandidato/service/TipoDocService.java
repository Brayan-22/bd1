package com.db1.modulocandidato.service;

import java.util.List;

import com.db1.modulocandidato.models.TipoDoc;

public interface TipoDocService {
    public List<TipoDoc> getAll();
}
