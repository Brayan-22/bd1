package com.db1.modulocandidato.repository.Impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.db1.modulocandidato.models.TipoDoc;
import com.db1.modulocandidato.repository.TipoDocRepository;

@Repository
public class TipoDocRepositoryImpl implements TipoDocRepository{

    private final JdbcTemplate plantilla;
    public TipoDocRepositoryImpl(JdbcTemplate jdbcTemplate){
        this.plantilla = jdbcTemplate;
    }
    RowMapper<TipoDoc> rowMapper = (rs,rowNum) ->{
        return new TipoDoc(rs.getString("IDTIPODOC"), rs.getString("DESCTIPODOC"));
    };
    @Override
    public List<TipoDoc> getAll() {
        String sql = "select * from TIPODOC";
        return plantilla.query(sql, rowMapper);
    }
    
}
