/*==============================================================*/
/* DBMS name:      ORACLE Version 12c                           */
/* Created on:     19/04/2024 8:31:38 p. m.                     */
/*==============================================================*/


alter table CANDIDATO
   drop constraint FK_CANDIDAT_TIPODOC_C_TIPODOC;

drop index TIPODOC_CANDIDATO_FK;

drop table CANDIDATO cascade constraints;

drop table TIPODOC cascade constraints;

/*==============================================================*/
/* Table: CANDIDATO                                             */
/*==============================================================*/
create table CANDIDATO (
   USUARIO              VARCHAR2(30)          not null,
   IDTIPODOC            VARCHAR2(3)           not null,
   NOMBRE               VARCHAR2(30)          not null,
   APELLIDO             VARCHAR2(30)          not null,
   FECHANAC             DATE                  not null,
   NDOC                 NUMBER(15)            not null,
   constraint PK_CANDIDATO primary key (USUARIO)
);

comment on table CANDIDATO is
'Entidad de candidato';

/*==============================================================*/
/* Index: TIPODOC_CANDIDATO_FK                                  */
/*==============================================================*/
create index TIPODOC_CANDIDATO_FK on CANDIDATO (
   IDTIPODOC ASC
);

/*==============================================================*/
/* Table: TIPODOC                                               */
/*==============================================================*/
create table TIPODOC (
   IDTIPODOC            VARCHAR2(3)           not null,
   DESCTIPODOC          VARCHAR2(20)          not null,
   constraint PK_TIPODOC primary key (IDTIPODOC)
);

comment on table TIPODOC is
'Entidad para el tipo de documento';

alter table CANDIDATO
   add constraint FK_CANDIDAT_TIPODOC_C_TIPODOC foreign key (IDTIPODOC)
      references TIPODOC (IDTIPODOC);

