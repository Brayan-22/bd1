/*==============================================================*/
/* DBMS name:      ORACLE Version 12c                           */
/* Created on:     27/05/2024 12:18:13 a.�m.                    */
/*==============================================================*/


alter table CANDIDATO
   drop constraint FK_CANDIDAT_DISCCAND_DISCIPLI;

alter table CANDIDATO
   drop constraint FK_CANDIDAT_TIPDOCCAN_TIPODOC;

alter table CARGO
   drop constraint FK_CARGO_EMPLCARGO_EMPLEADO;

alter table CARGO
   drop constraint FK_CARGO_TIPCARG_TIPOCARG;

alter table CONTACTOCANDIDATO
   drop constraint FK_CONTACTO_CANDCONTA_CANDIDAT;

alter table CONTACTOCANDIDATO
   drop constraint FK_CONTACTO_TIPCONTCA_TIPOCONT;

alter table CONTACTOCLIENTE
   drop constraint FK_CONTACTO_CLIENTCON_CLIENTE;

alter table CONTACTOCLIENTE
   drop constraint FK_CONTACTO_TIPCARGCO_TIPOCARG;

alter table CONTACTOCLIENTE
   drop constraint FK_CONTACTO_TIPCONTAC_TIPOCONT;

alter table DETALLEFACTURA
   drop constraint FK_DETALLEF_DETFACT_FACTURA;

alter table FACTURA
   drop constraint FK_FACTURA_CLIENTFAC_CLIENTE;

alter table FACTURA
   drop constraint FK_FACTURA_DIRECTORC_EMPLEADO;

alter table FASECARGO
   drop constraint FK_FASECARG_FAS_FASCA_FASE;

alter table FASECARGO
   drop constraint FK_FASECARG_TIPCARGOF_TIPOCARG;

alter table HV
   drop constraint FK_HV_CANDHV_CANDIDAT;

alter table HV
   drop constraint FK_HV_INSTHV_INSTITUC;

alter table HV
   drop constraint FK_HV_TIPITEMPE_TIPOITEM;

alter table ITEMPERFIL
   drop constraint FK_ITEMPERF_PERF_ITPE_PERFIL;

alter table ITEMPERFIL
   drop constraint FK_ITEMPERF_TIPITPERF_TIPOITEM;

alter table PERFIL
   drop constraint FK_PERFIL_DISCPERF_DISCIPLI;

alter table PERFILFASE
   drop constraint FK_PERFILFA_FASEP_FASE;

alter table PERFILFASE
   drop constraint FK_PERFILFA_PERFILF_PERFIL;

alter table PREGUNTA
   drop constraint FK_PREGUNTA_PRUEBPREG_PRUEBA;

alter table PREGUNTA
   drop constraint FK_PREGUNTA_TIPPREG_TIPOPREG;

alter table PREGUNTACANDIDATO
   drop constraint FK_PREGUNTA_PRBCANDPR_PRUEBACA;

alter table PREGUNTACANDIDATO
   drop constraint FK_PREGUNTA_PREG_PREG_PREGUNTA;

alter table PROCESOCANDIDATO
   drop constraint FK_PROCESOC_CANDPROC_CANDIDAT;

alter table PROCESOCANDIDATO
   drop constraint FK_PROCESOC_PROCESO_PROCESOR;

alter table PROCESOREQUERIMIENTO
   drop constraint FK_PROCESOR_PERFASEPR_PERFILFA;

alter table PROCESOREQUERIMIENTO
   drop constraint FK_PROCESOR_REQ_PROCR_REQUERIM;

alter table PROCESOREQUERIMIENTO
   drop constraint FK_PROCESOR_RESPONSAB_EMPLEADO;

alter table PRUEBA
   drop constraint FK_PRUEBA_DISCPRUEB_DISCIPLI;

alter table PRUEBA
   drop constraint FK_PRUEBA_FASE_PRUE_FASE;

alter table PRUEBA
   drop constraint FK_PRUEBA_TIPPRUEBA_TIPOPRUE;

alter table PRUEBACANDIDATO
   drop constraint FK_PRUEBACA_PROCCAND__PROCESOC;

alter table PRUEBACANDIDATO
   drop constraint FK_PRUEBACA_PRUEBCAND_PRUEBA;

alter table REQUERIMIENTO
   drop constraint FK_REQUERIM_EMPLREQ_EMPLEADO;

alter table RESPUESTA
   drop constraint FK_RESPUEST_PREGRESP_PREGUNTA;

alter table RESPUESTACANDIDATO
   drop constraint FK_RESPUEST_PREGCAND_PREGUNTA;

drop index DISCCAND_FK;

drop index TIPDOCCAND_FK;

drop table CANDIDATO cascade constraints;

drop index TIPCARG_FK;

drop index EMPLCARGO_FK;

drop table CARGO cascade constraints;

drop table CLIENTE cascade constraints;

drop index TIPCONTCAND_FK;

drop index CANDCONTACTO_FK;

drop table CONTACTOCANDIDATO cascade constraints;

drop index TIPCARGCONTACTCLIENT_FK;

drop index TIPCONTACT_FK;

drop index CLIENTCONTACT_FK;

drop table CONTACTOCLIENTE cascade constraints;

drop index DETFACT_FK;

drop table DETALLEFACTURA cascade constraints;

drop table DISCIPLINA cascade constraints;

drop table EMPLEADO cascade constraints;

drop index DIRECTORCUENTA_FK;

drop index CLIENTFACT_FK;

drop table FACTURA cascade constraints;

drop table FASE cascade constraints;

drop index FAS_FASCARG_FK;

drop index TIPCARGOFASE_FK;

drop table FASECARGO cascade constraints;

drop index TIPITEMPERFHV_FK;

drop index INSTHV_FK;

drop index CANDHV_FK;

drop table HV cascade constraints;

drop table INSTITUCION cascade constraints;

drop index TIPITPERF_FK;

drop index PERF_ITPERF_FK;

drop table ITEMPERFIL cascade constraints;

drop index DISCPERF_FK;

drop table PERFIL cascade constraints;

drop index FASEP_FK;

drop index PERFILF_FK;

drop index PERFILFASE_PK;

drop table PERFILFASE cascade constraints;

drop index TIPPREG_FK;

drop index PRUEBPREG_FK;

drop table PREGUNTA cascade constraints;

drop index PRBCANDPREG_FK;

drop index PREG_PREGCAND_FK;

drop index PREGUNTACANDIDATO_PK;

drop table PREGUNTACANDIDATO cascade constraints;

drop index CANDPROC_FK;

drop index PROCESOCANDIDATO_PK;

drop table PROCESOCANDIDATO cascade constraints;

drop index RESPONSABLE_FK;

drop index REQ_PROCREQ_FK;

drop index PERFASEPROCREQ_FK;

drop table PROCESOREQUERIMIENTO cascade constraints;

drop index TIPPRUEBA_FK;

drop index DISCPRUEB_FK;

drop index FASE_PRUE_FK;

drop table PRUEBA cascade constraints;

drop index PROCCAND_PRUEBA_FK;

drop index PRUEBCANDI_FK;

drop table PRUEBACANDIDATO cascade constraints;

drop index EMPLREQ_FK;

drop table REQUERIMIENTO cascade constraints;

drop index PREGRESP_FK;

drop table RESPUESTA cascade constraints;

drop index PREGCAND_FK;

drop table RESPUESTACANDIDATO cascade constraints;

drop table TIPOCARGO cascade constraints;

drop table TIPOCONTACTO cascade constraints;

drop table TIPODOC cascade constraints;

drop table TIPOITEMPERFIL cascade constraints;

drop table TIPOPREGUNTA cascade constraints;

drop table TIPOPRUEBA cascade constraints;

/*==============================================================*/
/* Table: CANDIDATO                                             */
/*==============================================================*/
create table CANDIDATO (
   USUARIO              VARCHAR2(30)          not null,
   IDTIPODOC            VARCHAR2(3)           not null,
   IDDISCIPLINA         VARCHAR2(4)           not null,
   NOMBRE               VARCHAR2(30)          not null,
   APELLIDO             VARCHAR2(30)          not null,
   FECHANAC             DATE                  not null,
   NDOC                 NUMBER(15)            not null,
   constraint PK_CANDIDATO primary key (USUARIO)
);

/*==============================================================*/
/* Index: TIPDOCCAND_FK                                         */
/*==============================================================*/
create index TIPDOCCAND_FK on CANDIDATO (
   IDTIPODOC ASC
);

/*==============================================================*/
/* Index: DISCCAND_FK                                           */
/*==============================================================*/
create index DISCCAND_FK on CANDIDATO (
   IDDISCIPLINA ASC
);

/*==============================================================*/
/* Table: CARGO                                                 */
/*==============================================================*/
create table CARGO (
   CONSECARGO           NUMBER(4,0)           not null,
   CODEMPLEADO          VARCHAR2(5)           not null,
   IDTIPOCARGO          VARCHAR2(3)           not null,
   FECHAINICIOCARGO     DATE                  not null,
   FECHAFINCARGO        DATE,
   DESCCARGO            VARCHAR2(30)          not null,
   constraint PK_CARGO primary key (CONSECARGO)
);

/*==============================================================*/
/* Index: EMPLCARGO_FK                                          */
/*==============================================================*/
create index EMPLCARGO_FK on CARGO (
   CODEMPLEADO ASC
);

/*==============================================================*/
/* Index: TIPCARG_FK                                            */
/*==============================================================*/
create index TIPCARG_FK on CARGO (
   IDTIPOCARGO ASC
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   NIT                  NUMBER(12,0)          not null,
   RAZONSOCIAL          VARCHAR2(40)          not null,
   URL                  VARCHAR2(30)          not null,
   constraint PK_CLIENTE primary key (NIT)
);

/*==============================================================*/
/* Table: CONTACTOCANDIDATO                                     */
/*==============================================================*/
create table CONTACTOCANDIDATO (
   CONSECONTACANDI      NUMBER(4)             not null,
   USUARIO              VARCHAR2(30)          not null,
   IDTIPOCONTACTO       VARCHAR2(3)           not null,
   constraint PK_CONTACTOCANDIDATO primary key (CONSECONTACANDI)
);

/*==============================================================*/
/* Index: CANDCONTACTO_FK                                       */
/*==============================================================*/
create index CANDCONTACTO_FK on CONTACTOCANDIDATO (
   USUARIO ASC
);

/*==============================================================*/
/* Index: TIPCONTCAND_FK                                        */
/*==============================================================*/
create index TIPCONTCAND_FK on CONTACTOCANDIDATO (
   IDTIPOCONTACTO ASC
);

/*==============================================================*/
/* Table: CONTACTOCLIENTE                                       */
/*==============================================================*/
create table CONTACTOCLIENTE (
   NIT                  NUMBER(12,0)          not null,
   CONSECONTACLIENTE    NUMBER(3,0)           not null,
   IDTIPOCONTACTO       VARCHAR2(3)           not null,
   IDTIPOCARGO          VARCHAR2(3)           not null,
   NOMBREAPELLIDOCLIEN  VARCHAR2(30)          not null,
   ACTIVOCONTACLIENTE   SMALLINT              not null,
   constraint PK_CONTACTOCLIENTE primary key (NIT, CONSECONTACLIENTE)
);

/*==============================================================*/
/* Index: CLIENTCONTACT_FK                                      */
/*==============================================================*/
create index CLIENTCONTACT_FK on CONTACTOCLIENTE (
   NIT ASC
);

/*==============================================================*/
/* Index: TIPCONTACT_FK                                         */
/*==============================================================*/
create index TIPCONTACT_FK on CONTACTOCLIENTE (
   IDTIPOCONTACTO ASC
);

/*==============================================================*/
/* Index: TIPCARGCONTACTCLIENT_FK                               */
/*==============================================================*/
create index TIPCARGCONTACTCLIENT_FK on CONTACTOCLIENTE (
   IDTIPOCARGO ASC
);

/*==============================================================*/
/* Table: DETALLEFACTURA                                        */
/*==============================================================*/
create table DETALLEFACTURA (
   NFACTURA             VARCHAR2(6)           not null,
   ITEM                 NUMBER(4,0)           not null,
   constraint PK_DETALLEFACTURA primary key (NFACTURA, ITEM)
);

/*==============================================================*/
/* Index: DETFACT_FK                                            */
/*==============================================================*/
create index DETFACT_FK on DETALLEFACTURA (
   NFACTURA ASC
);

/*==============================================================*/
/* Table: DISCIPLINA                                            */
/*==============================================================*/
create table DISCIPLINA (
   IDDISCIPLINA         VARCHAR2(4)           not null,
   DESCDISCIPLINA       VARCHAR2(30)          not null,
   constraint PK_DISCIPLINA primary key (IDDISCIPLINA)
);

/*==============================================================*/
/* Table: EMPLEADO                                              */
/*==============================================================*/
create table EMPLEADO (
   CODEMPLEADO          VARCHAR2(5)           not null,
   NOMEMPLEADO          VARCHAR2(30)          not null,
   APELLEMPLEADO        VARCHAR2(30)          not null,
   FECHANAC             DATE                  not null,
   FECHAINGRE           DATE                  not null,
   FECHAEGRESO          DATE,
   CORREO               VARCHAR2(30)          not null,
   constraint PK_EMPLEADO primary key (CODEMPLEADO)
);

/*==============================================================*/
/* Table: FACTURA                                               */
/*==============================================================*/
create table FACTURA (
   NFACTURA             VARCHAR2(6)           not null,
   NIT                  NUMBER(12,0)          not null,
   CODEMPLEADO          VARCHAR2(5)           not null,
   FECHAFACTURA         DATE                  not null,
   constraint PK_FACTURA primary key (NFACTURA)
);

/*==============================================================*/
/* Index: CLIENTFACT_FK                                         */
/*==============================================================*/
create index CLIENTFACT_FK on FACTURA (
   NIT ASC
);

/*==============================================================*/
/* Index: DIRECTORCUENTA_FK                                     */
/*==============================================================*/
create index DIRECTORCUENTA_FK on FACTURA (
   CODEMPLEADO ASC
);

/*==============================================================*/
/* Table: FASE                                                  */
/*==============================================================*/
create table FASE (
   IDFASE               VARCHAR2(4)           not null,
   DESFASE              VARCHAR2(25)          not null,
   constraint PK_FASE primary key (IDFASE)
);

/*==============================================================*/
/* Table: FASECARGO                                             */
/*==============================================================*/
create table FASECARGO (
   CONSEFASECARGO       NUMBER(4,0)           not null,
   IDTIPOCARGO          VARCHAR2(3),
   IDFASE               VARCHAR2(4),
   constraint PK_FASECARGO primary key (CONSEFASECARGO)
);

/*==============================================================*/
/* Index: TIPCARGOFASE_FK                                       */
/*==============================================================*/
create index TIPCARGOFASE_FK on FASECARGO (
   IDTIPOCARGO ASC
);

/*==============================================================*/
/* Index: FAS_FASCARG_FK                                        */
/*==============================================================*/
create index FAS_FASCARG_FK on FASECARGO (
   IDFASE ASC
);

/*==============================================================*/
/* Table: HV                                                    */
/*==============================================================*/
create table HV (
   CONSEHV              NUMBER(3,0)           not null,
   USUARIO              VARCHAR2(30)          not null,
   CODINSTITUCION       NUMBER(5,0)           not null,
   IDTIPOITEMPERFIL     VARCHAR2(4)           not null,
   FECHAINIACT          DATE                  not null,
   FECHAFINACT          DATE,
   DESCACTIVIDAD        VARCHAR2(50)          not null,
   FUNCIONACTIVIDAD     VARCHAR2(50),
   constraint PK_HV primary key (CONSEHV)
);

/*==============================================================*/
/* Index: CANDHV_FK                                             */
/*==============================================================*/
create index CANDHV_FK on HV (
   USUARIO ASC
);

/*==============================================================*/
/* Index: INSTHV_FK                                             */
/*==============================================================*/
create index INSTHV_FK on HV (
   CODINSTITUCION ASC
);

/*==============================================================*/
/* Index: TIPITEMPERFHV_FK                                      */
/*==============================================================*/
create index TIPITEMPERFHV_FK on HV (
   IDTIPOITEMPERFIL ASC
);

/*==============================================================*/
/* Table: INSTITUCION                                           */
/*==============================================================*/
create table INSTITUCION (
   CODINSTITUCION       NUMBER(5,0)           not null,
   NOMINSTITUCION       VARCHAR2(40)          not null,
   constraint PK_INSTITUCION primary key (CODINSTITUCION)
);

/*==============================================================*/
/* Table: ITEMPERFIL                                            */
/*==============================================================*/
create table ITEMPERFIL (
   IDITEM               NUMBER(4,0)           not null,
   IDPERFIL             VARCHAR2(4)           not null,
   IDTIPOITEMPERFIL     VARCHAR2(4)           not null,
   DESCITEM             VARCHAR2(30)          not null,
   constraint PK_ITEMPERFIL primary key (IDITEM)
);

/*==============================================================*/
/* Index: PERF_ITPERF_FK                                        */
/*==============================================================*/
create index PERF_ITPERF_FK on ITEMPERFIL (
   IDPERFIL ASC
);

/*==============================================================*/
/* Index: TIPITPERF_FK                                          */
/*==============================================================*/
create index TIPITPERF_FK on ITEMPERFIL (
   IDTIPOITEMPERFIL ASC
);

/*==============================================================*/
/* Table: PERFIL                                                */
/*==============================================================*/
create table PERFIL (
   IDPERFIL             VARCHAR2(4)           not null,
   IDDISCIPLINA         VARCHAR2(4)           not null,
   DESPERFIL            VARCHAR2(50)          not null,
   constraint PK_PERFIL primary key (IDPERFIL)
);

/*==============================================================*/
/* Index: DISCPERF_FK                                           */
/*==============================================================*/
create index DISCPERF_FK on PERFIL (
   IDDISCIPLINA ASC
);

/*==============================================================*/
/* Table: PERFILFASE                                            */
/*==============================================================*/
create table PERFILFASE (
   IDFASE               VARCHAR2(4)           not null,
   IDPERFIL             VARCHAR2(4)           not null,
   constraint PK_PERFILFASE primary key (IDFASE, IDPERFIL)
);

/*==============================================================*/
/* Index: PERFILFASE_PK                                         */
/*==============================================================*/
create unique index PERFILFASE_PK on PERFILFASE (
   IDFASE ASC,
   IDPERFIL ASC
);

/*==============================================================*/
/* Index: PERFILF_FK                                            */
/*==============================================================*/
create index PERFILF_FK on PERFILFASE (
   IDPERFIL ASC
);

/*==============================================================*/
/* Index: FASEP_FK                                              */
/*==============================================================*/
create index FASEP_FK on PERFILFASE (
   IDFASE ASC
);

/*==============================================================*/
/* Table: PREGUNTA                                              */
/*==============================================================*/
create table PREGUNTA (
   IDPRUEBA             VARCHAR2(5)           not null,
   CONSEPREGUNTA        NUMBER(5,0)           not null,
   IDTIPOPREGUNTA       VARCHAR2(4)           not null,
   DESCPREGUNTA         VARCHAR2(30)          not null,
   constraint PK_PREGUNTA primary key (IDPRUEBA, CONSEPREGUNTA)
);

/*==============================================================*/
/* Index: PRUEBPREG_FK                                          */
/*==============================================================*/
create index PRUEBPREG_FK on PREGUNTA (
   IDPRUEBA ASC
);

/*==============================================================*/
/* Index: TIPPREG_FK                                            */
/*==============================================================*/
create index TIPPREG_FK on PREGUNTA (
   IDTIPOPREGUNTA ASC
);

/*==============================================================*/
/* Table: PREGUNTACANDIDATO                                     */
/*==============================================================*/
create table PREGUNTACANDIDATO (
   CONSEPRUEBACANDI     NUMBER(5,0)           not null,
   IDPRUEBA             VARCHAR2(5)           not null,
   CONSEPREGUNTA        NUMBER(5,0)           not null,
   constraint PK_PREGUNTACANDIDATO primary key (CONSEPRUEBACANDI, IDPRUEBA, CONSEPREGUNTA)
);

/*==============================================================*/
/* Index: PREGUNTACANDIDATO_PK                                  */
/*==============================================================*/
create unique index PREGUNTACANDIDATO_PK on PREGUNTACANDIDATO (
   CONSEPRUEBACANDI ASC,
   IDPRUEBA ASC,
   CONSEPREGUNTA ASC
);

/*==============================================================*/
/* Index: PREG_PREGCAND_FK                                      */
/*==============================================================*/
create index PREG_PREGCAND_FK on PREGUNTACANDIDATO (
   IDPRUEBA ASC,
   CONSEPREGUNTA ASC
);

/*==============================================================*/
/* Index: PRBCANDPREG_FK                                        */
/*==============================================================*/
create index PRBCANDPREG_FK on PREGUNTACANDIDATO (
   CONSEPRUEBACANDI ASC
);

/*==============================================================*/
/* Table: PROCESOCANDIDATO                                      */
/*==============================================================*/
create table PROCESOCANDIDATO (
   USUARIO              VARCHAR2(30)          not null,
   CONSECREQUE          NUMBER(5,0)           not null,
   IDFASE               VARCHAR2(4)           not null,
   IDPERFIL             VARCHAR2(4)           not null,
   CONSPROCESO          NUMBER(5,0)           not null,
   FECHAPRESENTACION    DATE                  not null,
   ANALISIS             VARCHAR2(50),
   OBSERVACION          VARCHAR2(50),
   constraint PK_PROCESOCANDIDATO primary key (USUARIO, CONSECREQUE, IDFASE, IDPERFIL, CONSPROCESO)
);

/*==============================================================*/
/* Index: PROCESOCANDIDATO_PK                                   */
/*==============================================================*/
create unique index PROCESOCANDIDATO_PK on PROCESOCANDIDATO (
   CONSECREQUE ASC,
   IDFASE ASC,
   IDPERFIL ASC,
   CONSPROCESO ASC
);

/*==============================================================*/
/* Index: CANDPROC_FK                                           */
/*==============================================================*/
create index CANDPROC_FK on PROCESOCANDIDATO (
   USUARIO ASC
);

/*==============================================================*/
/* Table: PROCESOREQUERIMIENTO                                  */
/*==============================================================*/
create table PROCESOREQUERIMIENTO (
   CONSECREQUE          NUMBER(5,0)           not null,
   IDFASE               VARCHAR2(4)           not null,
   IDPERFIL             VARCHAR2(4)           not null,
   CONSPROCESO          NUMBER(5,0)           not null,
   CODEMPLEADO          VARCHAR2(5),
   FECHAINICIO          DATE,
   FECHAFIN             DATE,
   CONVOCATORIA         VARCHAR2(200),
   INVITACION           VARCHAR2(200),
   constraint PK_PROCESOREQUERIMIENTO primary key (CONSECREQUE, IDFASE, IDPERFIL, CONSPROCESO)
);

/*==============================================================*/
/* Index: PERFASEPROCREQ_FK                                     */
/*==============================================================*/
create index PERFASEPROCREQ_FK on PROCESOREQUERIMIENTO (
   IDFASE ASC,
   IDPERFIL ASC
);

/*==============================================================*/
/* Index: REQ_PROCREQ_FK                                        */
/*==============================================================*/
create index REQ_PROCREQ_FK on PROCESOREQUERIMIENTO (
   CONSECREQUE ASC
);

/*==============================================================*/
/* Index: RESPONSABLE_FK                                        */
/*==============================================================*/
create index RESPONSABLE_FK on PROCESOREQUERIMIENTO (
   CODEMPLEADO ASC
);

/*==============================================================*/
/* Table: PRUEBA                                                */
/*==============================================================*/
create table PRUEBA (
   IDPRUEBA             VARCHAR2(5)           not null,
   IDFASE               VARCHAR2(4)           not null,
   IDDISCIPLINA         VARCHAR2(4),
   IDTIPOPRUEBA         VARCHAR2(2)           not null,
   DESCPRUEBA           VARCHAR2(30)          not null,
   PRUEBAACTIVA         NUMBER(1,0)           not null,
   FECHACREADA          DATE,
   constraint PK_PRUEBA primary key (IDPRUEBA),
   CONSTRAINT chk_activo CHECK (PRUEBAACTIVA IN (0, 1))
);

/*==============================================================*/
/* Index: FASE_PRUE_FK                                          */
/*==============================================================*/
create index FASE_PRUE_FK on PRUEBA (
   IDFASE ASC
);

/*==============================================================*/
/* Index: DISCPRUEB_FK                                          */
/*==============================================================*/
create index DISCPRUEB_FK on PRUEBA (
   IDDISCIPLINA ASC
);

/*==============================================================*/
/* Index: TIPPRUEBA_FK                                          */
/*==============================================================*/
create index TIPPRUEBA_FK on PRUEBA (
   IDTIPOPRUEBA ASC
);

/*==============================================================*/
/* Table: PRUEBACANDIDATO                                       */
/*==============================================================*/
create table PRUEBACANDIDATO (
   CONSEPRUEBACANDI     NUMBER(5,0)           not null,
   IDPRUEBA             VARCHAR2(5),
   USUARIO              VARCHAR2(30)          not null,
   CONSECREQUE          NUMBER(5,0)           not null,
   IDFASE               VARCHAR2(4)           not null,
   IDPERFIL             VARCHAR2(4)           not null,
   CONSPROCESO          NUMBER(5,0)           not null,
   FECHAPRES            DATE                  not null,
   CALIFICACION         NUMBER(3,1),
   constraint PK_PRUEBACANDIDATO primary key (CONSEPRUEBACANDI)
);

/*==============================================================*/
/* Index: PRUEBCANDI_FK                                         */
/*==============================================================*/
create index PRUEBCANDI_FK on PRUEBACANDIDATO (
   IDPRUEBA ASC
);

/*==============================================================*/
/* Index: PROCCAND_PRUEBA_FK                                    */
/*==============================================================*/
create index PROCCAND_PRUEBA_FK on PRUEBACANDIDATO (
   USUARIO ASC,
   CONSECREQUE ASC,
   IDFASE ASC,
   IDPERFIL ASC,
   CONSPROCESO ASC
);

/*==============================================================*/
/* Table: REQUERIMIENTO                                         */
/*==============================================================*/
create table REQUERIMIENTO (
   CONSECREQUE          NUMBER(5,0)           not null,
   CODEMPLEADO          VARCHAR2(5),
   FECHAREQUE           DATE                  not null,
   SALARIOMAX           NUMBER(6,0)           not null,
   SALARIOMIN           NUMBER(6,0),
   DESFUNCION           VARCHAR2(50)          not null,
   NVACANTES            NUMBER(2,0)           not null,
   constraint PK_REQUERIMIENTO primary key (CONSECREQUE)
);

/*==============================================================*/
/* Index: EMPLREQ_FK                                            */
/*==============================================================*/
create index EMPLREQ_FK on REQUERIMIENTO (
   CODEMPLEADO ASC
);

/*==============================================================*/
/* Table: RESPUESTA                                             */
/*==============================================================*/
create table RESPUESTA (
   IDPRUEBA             VARCHAR2(5)           not null,
   CONSEPREGUNTA        NUMBER(5,0)           not null,
   CONSECRESPUESTA      NUMBER(3,0)           not null,
   RESPUESTA            VARCHAR2(30),
   constraint PK_RESPUESTA primary key (IDPRUEBA, CONSEPREGUNTA, CONSECRESPUESTA)
);

/*==============================================================*/
/* Index: PREGRESP_FK                                           */
/*==============================================================*/
create index PREGRESP_FK on RESPUESTA (
   IDPRUEBA ASC,
   CONSEPREGUNTA ASC
);

/*==============================================================*/
/* Table: RESPUESTACANDIDATO                                    */
/*==============================================================*/
create table RESPUESTACANDIDATO (
   CONSECRESCANDI       NUMBER(4,0)           not null,
   CONSEPRUEBACANDI     NUMBER(5,0),
   IDPRUEBA             VARCHAR2(5),
   CONSEPREGUNTA        NUMBER(5,0),
   RESCANDI             VARCHAR2(40)          not null,
   constraint PK_RESPUESTACANDIDATO primary key (CONSECRESCANDI)
);

/*==============================================================*/
/* Index: PREGCAND_FK                                           */
/*==============================================================*/
create index PREGCAND_FK on RESPUESTACANDIDATO (
   CONSEPRUEBACANDI ASC,
   IDPRUEBA ASC,
   CONSEPREGUNTA ASC
);

/*==============================================================*/
/* Table: TIPOCARGO                                             */
/*==============================================================*/
create table TIPOCARGO (
   IDTIPOCARGO          VARCHAR2(3)           not null,
   DESCTIPOCARGO        VARCHAR2(20)          not null,
   constraint PK_TIPOCARGO primary key (IDTIPOCARGO)
);

/*==============================================================*/
/* Table: TIPOCONTACTO                                          */
/*==============================================================*/
create table TIPOCONTACTO (
   IDTIPOCONTACTO       VARCHAR2(3)           not null,
   DESCTIPOCONTACTO     VARCHAR2(20)          not null,
   constraint PK_TIPOCONTACTO primary key (IDTIPOCONTACTO)
);

/*==============================================================*/
/* Table: TIPODOC                                               */
/*==============================================================*/
create table TIPODOC (
   IDTIPODOC            VARCHAR2(3)           not null,
   DESCTIPODOC          VARCHAR2(20)          not null,
   constraint PK_TIPODOC primary key (IDTIPODOC)
);

/*==============================================================*/
/* Table: TIPOITEMPERFIL                                        */
/*==============================================================*/
create table TIPOITEMPERFIL (
   IDTIPOITEMPERFIL     VARCHAR2(4)           not null,
   DESCSTIPOITEMPERFIL  VARCHAR2(30),
   constraint PK_TIPOITEMPERFIL primary key (IDTIPOITEMPERFIL)
);

/*==============================================================*/
/* Table: TIPOPREGUNTA                                          */
/*==============================================================*/
create table TIPOPREGUNTA (
   IDTIPOPREGUNTA       VARCHAR2(4)           not null,
   DESCTIPOPREGUNTA     VARCHAR2(30)          not null,
   constraint PK_TIPOPREGUNTA primary key (IDTIPOPREGUNTA)
);

/*==============================================================*/
/* Table: TIPOPRUEBA                                            */
/*==============================================================*/
create table TIPOPRUEBA (
   IDTIPOPRUEBA         VARCHAR2(2)           not null,
   DESCTIPOPRUEBA       VARCHAR2(30)          not null,
   constraint PK_TIPOPRUEBA primary key (IDTIPOPRUEBA)
);

alter table CANDIDATO
   add constraint FK_CANDIDAT_DISCCAND_DISCIPLI foreign key (IDDISCIPLINA)
      references DISCIPLINA (IDDISCIPLINA);

alter table CANDIDATO
   add constraint FK_CANDIDAT_TIPDOCCAN_TIPODOC foreign key (IDTIPODOC)
      references TIPODOC (IDTIPODOC);

alter table CARGO
   add constraint FK_CARGO_EMPLCARGO_EMPLEADO foreign key (CODEMPLEADO)
      references EMPLEADO (CODEMPLEADO);

alter table CARGO
   add constraint FK_CARGO_TIPCARG_TIPOCARG foreign key (IDTIPOCARGO)
      references TIPOCARGO (IDTIPOCARGO);

alter table CONTACTOCANDIDATO
   add constraint FK_CONTACTO_CANDCONTA_CANDIDAT foreign key (USUARIO)
      references CANDIDATO (USUARIO);

alter table CONTACTOCANDIDATO
   add constraint FK_CONTACTO_TIPCONTCA_TIPOCONT foreign key (IDTIPOCONTACTO)
      references TIPOCONTACTO (IDTIPOCONTACTO);

alter table CONTACTOCLIENTE
   add constraint FK_CONTACTO_CLIENTCON_CLIENTE foreign key (NIT)
      references CLIENTE (NIT);

alter table CONTACTOCLIENTE
   add constraint FK_CONTACTO_TIPCARGCO_TIPOCARG foreign key (IDTIPOCARGO)
      references TIPOCARGO (IDTIPOCARGO);

alter table CONTACTOCLIENTE
   add constraint FK_CONTACTO_TIPCONTAC_TIPOCONT foreign key (IDTIPOCONTACTO)
      references TIPOCONTACTO (IDTIPOCONTACTO);

alter table DETALLEFACTURA
   add constraint FK_DETALLEF_DETFACT_FACTURA foreign key (NFACTURA)
      references FACTURA (NFACTURA);

alter table FACTURA
   add constraint FK_FACTURA_CLIENTFAC_CLIENTE foreign key (NIT)
      references CLIENTE (NIT);

alter table FACTURA
   add constraint FK_FACTURA_DIRECTORC_EMPLEADO foreign key (CODEMPLEADO)
      references EMPLEADO (CODEMPLEADO);

alter table FASECARGO
   add constraint FK_FASECARG_FAS_FASCA_FASE foreign key (IDFASE)
      references FASE (IDFASE);

alter table FASECARGO
   add constraint FK_FASECARG_TIPCARGOF_TIPOCARG foreign key (IDTIPOCARGO)
      references TIPOCARGO (IDTIPOCARGO);

alter table HV
   add constraint FK_HV_CANDHV_CANDIDAT foreign key (USUARIO)
      references CANDIDATO (USUARIO);

alter table HV
   add constraint FK_HV_INSTHV_INSTITUC foreign key (CODINSTITUCION)
      references INSTITUCION (CODINSTITUCION);

alter table HV
   add constraint FK_HV_TIPITEMPE_TIPOITEM foreign key (IDTIPOITEMPERFIL)
      references TIPOITEMPERFIL (IDTIPOITEMPERFIL);

alter table ITEMPERFIL
   add constraint FK_ITEMPERF_PERF_ITPE_PERFIL foreign key (IDPERFIL)
      references PERFIL (IDPERFIL);

alter table ITEMPERFIL
   add constraint FK_ITEMPERF_TIPITPERF_TIPOITEM foreign key (IDTIPOITEMPERFIL)
      references TIPOITEMPERFIL (IDTIPOITEMPERFIL);

alter table PERFIL
   add constraint FK_PERFIL_DISCPERF_DISCIPLI foreign key (IDDISCIPLINA)
      references DISCIPLINA (IDDISCIPLINA);

alter table PERFILFASE
   add constraint FK_PERFILFA_FASEP_FASE foreign key (IDFASE)
      references FASE (IDFASE);

alter table PERFILFASE
   add constraint FK_PERFILFA_PERFILF_PERFIL foreign key (IDPERFIL)
      references PERFIL (IDPERFIL);

alter table PREGUNTA
   add constraint FK_PREGUNTA_PRUEBPREG_PRUEBA foreign key (IDPRUEBA)
      references PRUEBA (IDPRUEBA);

alter table PREGUNTA
   add constraint FK_PREGUNTA_TIPPREG_TIPOPREG foreign key (IDTIPOPREGUNTA)
      references TIPOPREGUNTA (IDTIPOPREGUNTA);

alter table PREGUNTACANDIDATO
   add constraint FK_PREGUNTA_PRBCANDPR_PRUEBACA foreign key (CONSEPRUEBACANDI)
      references PRUEBACANDIDATO (CONSEPRUEBACANDI);

alter table PREGUNTACANDIDATO
   add constraint FK_PREGUNTA_PREG_PREG_PREGUNTA foreign key (IDPRUEBA, CONSEPREGUNTA)
      references PREGUNTA (IDPRUEBA, CONSEPREGUNTA);

alter table PROCESOCANDIDATO
   add constraint FK_PROCESOC_CANDPROC_CANDIDAT foreign key (USUARIO)
      references CANDIDATO (USUARIO);

alter table PROCESOCANDIDATO
   add constraint FK_PROCESOC_PROCESO_PROCESOR foreign key (CONSECREQUE, IDFASE, IDPERFIL, CONSPROCESO)
      references PROCESOREQUERIMIENTO (CONSECREQUE, IDFASE, IDPERFIL, CONSPROCESO);

alter table PROCESOREQUERIMIENTO
   add constraint FK_PROCESOR_PERFASEPR_PERFILFA foreign key (IDFASE, IDPERFIL)
      references PERFILFASE (IDFASE, IDPERFIL);

alter table PROCESOREQUERIMIENTO
   add constraint FK_PROCESOR_REQ_PROCR_REQUERIM foreign key (CONSECREQUE)
      references REQUERIMIENTO (CONSECREQUE);

alter table PROCESOREQUERIMIENTO
   add constraint FK_PROCESOR_RESPONSAB_EMPLEADO foreign key (CODEMPLEADO)
      references EMPLEADO (CODEMPLEADO);

alter table PRUEBA
   add constraint FK_PRUEBA_DISCPRUEB_DISCIPLI foreign key (IDDISCIPLINA)
      references DISCIPLINA (IDDISCIPLINA);

alter table PRUEBA
   add constraint FK_PRUEBA_FASE_PRUE_FASE foreign key (IDFASE)
      references FASE (IDFASE);

alter table PRUEBA
   add constraint FK_PRUEBA_TIPPRUEBA_TIPOPRUE foreign key (IDTIPOPRUEBA)
      references TIPOPRUEBA (IDTIPOPRUEBA);

alter table PRUEBACANDIDATO
   add constraint FK_PRUEBACA_PROCCAND__PROCESOC foreign key (USUARIO, CONSECREQUE, IDFASE, IDPERFIL, CONSPROCESO)
      references PROCESOCANDIDATO (USUARIO, CONSECREQUE, IDFASE, IDPERFIL, CONSPROCESO);

alter table PRUEBACANDIDATO
   add constraint FK_PRUEBACA_PRUEBCAND_PRUEBA foreign key (IDPRUEBA)
      references PRUEBA (IDPRUEBA);

alter table REQUERIMIENTO
   add constraint FK_REQUERIM_EMPLREQ_EMPLEADO foreign key (CODEMPLEADO)
      references EMPLEADO (CODEMPLEADO);

alter table RESPUESTA
   add constraint FK_RESPUEST_PREGRESP_PREGUNTA foreign key (IDPRUEBA, CONSEPREGUNTA)
      references PREGUNTA (IDPRUEBA, CONSEPREGUNTA);

alter table RESPUESTACANDIDATO
   add constraint FK_RESPUEST_PREGCAND_PREGUNTA foreign key (CONSEPRUEBACANDI, IDPRUEBA, CONSEPREGUNTA)
      references PREGUNTACANDIDATO (CONSEPRUEBACANDI, IDPRUEBA, CONSEPREGUNTA);

