package com.bd1.proyectofinal.repository;

import com.bd1.proyectofinal.models.EmpleadoDTO;


import java.util.*;
public interface EmpleadoRepository {
    public EmpleadoDTO findByCorreo(String correo);
    public List<EmpleadoDTO> findByTipoCargo(String idTipoCargo);
}