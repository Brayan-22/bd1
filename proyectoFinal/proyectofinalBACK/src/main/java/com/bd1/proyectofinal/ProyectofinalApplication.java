package com.bd1.proyectofinal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bd1.proyectofinal.models.EmpleadoDTO;
import com.bd1.proyectofinal.models.RequerimientoDTO;
import com.bd1.proyectofinal.models.TipoCargoDTO;
import com.bd1.proyectofinal.repository.EmpleadoRepository;
import com.bd1.proyectofinal.repository.TipoCargoRepository;
import com.bd1.proyectofinal.services.RequerimientoService;

import java.util.*;
@SpringBootApplication
public class ProyectofinalApplication implements CommandLineRunner{
	
	@Autowired
	private EmpleadoRepository empleadoRepository;
	
	@Autowired
	private RequerimientoService requerimientoService;
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	public static void main(String[] args) {
		SpringApplication.run(ProyectofinalApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
	}

}
