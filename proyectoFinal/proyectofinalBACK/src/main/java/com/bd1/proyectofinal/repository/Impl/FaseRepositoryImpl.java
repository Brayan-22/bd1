package com.bd1.proyectofinal.repository.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.bd1.proyectofinal.models.FaseDTO;
import com.bd1.proyectofinal.repository.FaseRepository;

@Service
public class FaseRepositoryImpl implements FaseRepository{

    private final JdbcTemplate plantilla;

    public FaseRepositoryImpl(@Autowired JdbcTemplate plantilla){
        this.plantilla = plantilla;
    }

    private RowMapper<FaseDTO> rowMapper = (rs,rwNum)->{
        return new FaseDTO(rs.getString("idFase"), rs.getString("DESFASE"));
    };

    @Override
    public List<FaseDTO> getAllFaseByCargo(String idCargo) {
        String sql = "select F.IDFASE,F.DESFASE from faseCargo fc,fase f where fc.IDTIPOCARGO=? and fc.IDFASE=f.IDFASE";
        return plantilla.query(sql, rowMapper, idCargo);
    }
    
}
