package com.bd1.proyectofinal.models;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequerimientoDTO {
    private int consecReque;
    private String desFuncion;
    private String desCarreras;
    private LocalDate fechaReque;
    private int numVacantes;
    private String idResponsable;
    private String idEmplAsignado;
    private String disciplina;
    private String perfil;
}
