package com.bd1.proyectofinal.repository.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.bd1.proyectofinal.models.RequerimientoDTO;
import com.bd1.proyectofinal.repository.RequerimientoRepository;


@Service
public class RequerimientoRepositoryImpl implements RequerimientoRepository{


    private JdbcTemplate plantilla;

    public RequerimientoRepositoryImpl(@Autowired JdbcTemplate plantilla){
        this.plantilla = plantilla;
    }

    private RowMapper<RequerimientoDTO> rowMapper = (rs,rwNum)->{
        return RequerimientoDTO.builder().consecReque(rs.getInt("CONSECREQUE")).disciplina(rs.getString("DESCDISCIPLINA")).desFuncion(rs.getString("DESFUNCION")).perfil(rs.getString("DESPERFIL")).numVacantes(rs.getInt("NVACANTES")).build();
    };


    private int consec(){
        String sql = "select count(*) from requerimiento";
        return plantilla.queryForObject(sql, Integer.class);
    }
    private int consecProcess(){
        String sql = "select count(*) from PROCESOREQUERIMIENTO";
        return plantilla.queryForObject(sql, Integer.class);
    }
    @Override
    public void saveRequerimiento(RequerimientoDTO req,String fase) {
        int sigConsec = consec()+1;
        String sql = "INSERT INTO REQUERIMIENTO(CONSECREQUE,CODEMPLEADO,FECHAREQUE,SALARIOMAX,SALARIOMIN,DESFUNCION,NVACANTES) VALUES (?,?,?,?,?,?,?)";
        plantilla.update(sql,
        sigConsec,
        req.getIdEmplAsignado(),
        req.getFechaReque(),
        0,
        0,
        req.getDesFuncion(),
        req.getNumVacantes());
        int sigConsecP = consecProcess()+1;
        String sqlProceso = "INSERT INTO PROCESOREQUERIMIENTO(CONSPROCESO,CONSECREQUE,IDFASE,IDPERFIL,CODEMPLEADO,FECHAINICIO,FECHAFIN,CONVOCATORIA,INVITACION)VALUES (?,?,?,?,?,?,?,?,?)";
        plantilla.update(sqlProceso,
        sigConsecP,
        sigConsec,
        fase,
        "DSCO",
        req.getIdResponsable(),
        req.getFechaReque(),
        null,
        null,
        null
        );
    }


    @Override
    public List<RequerimientoDTO> findByCodEmpleado(String codEmpleado) {
        String sql = "SELECT R.CONSECREQUE,D.DESCDISCIPLINA,R.DESFUNCION,PF.DESPERFIL,R.NVACANTES FROM DISCIPLINA D,PERFIL PF,REQUERIMIENTO R,PROCESOREQUERIMIENTO PR WHERE R.CONSECREQUE=PR.CONSECREQUE AND R.CODEMPLEADO=? AND PF.IDPERFIL=PR.IDPERFIL AND PF.IDDISCIPLINA=D.IDDISCIPLINA";
        return plantilla.query(sql, rowMapper,codEmpleado);
    }
    
}
