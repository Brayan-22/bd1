package com.bd1.proyectofinal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bd1.proyectofinal.models.RequerimientoDTO;
import com.bd1.proyectofinal.repository.RequerimientoRepository;
import java.util.*;
@Service
public class RequerimientoService {

    @Autowired
    private RequerimientoRepository requerimientoRepository;

    public void saveRequerimiento(RequerimientoDTO req,String fase){
        requerimientoRepository.saveRequerimiento(req,fase);
    } 

    public List<RequerimientoDTO> finByCodEmpleado(String codEmpleado){
        return requerimientoRepository.findByCodEmpleado(codEmpleado);
    }

}
