package com.bd1.proyectofinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import com.bd1.proyectofinal.models.EmpleadoDTO;
import com.bd1.proyectofinal.models.FaseDTO;
import com.bd1.proyectofinal.models.RequerimientoDTO;
import com.bd1.proyectofinal.repository.FaseRepository;
import com.bd1.proyectofinal.services.EmpleadoService;
import com.bd1.proyectofinal.services.RequerimientoService;

import jakarta.servlet.http.HttpSession;

import java.time.LocalDate;
import java.util.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class FaseController {


    @Autowired
    private FaseRepository faseRepository;

    @Autowired
    private EmpleadoService empleadoService;

    @Autowired
    private RequerimientoService requerimientoService;

    @GetMapping("/seleccionarFase")
    public String mostrarFases(HttpSession session,Model model) {
        EmpleadoDTO empleado = (EmpleadoDTO)session.getAttribute("empleado");
        if (empleado == null) {
            return "redirect:/login";
        }
        List<FaseDTO> fases = faseRepository.getAllFaseByCargo(empleado.getIdTipoCargo());
        model.addAttribute("fases",fases);
        model.addAttribute("empleado",empleado);
        return "seleccionarFase";
    }

    @GetMapping("/cambiar-fase")
    public String cambiarFase(@RequestParam int fase,HttpSession session,Model model) {
        EmpleadoDTO emp = (EmpleadoDTO) session.getAttribute("empleado");
        if (emp == null) {
            return "redirect:/login";
        }
        session.setAttribute("empleado", emp);
        session.setAttribute("fase", fase);
        return "redirect:/fase-"+fase;
    }
    

    @GetMapping("/fase-1")
    public String getMethodName(HttpSession session,Model model) {
        EmpleadoDTO emp = (EmpleadoDTO) session.getAttribute("empleado");
        if (emp == null) {
            return "redirect:/login";
        }
        model.addAttribute("fase","fase1");
        model.addAttribute("empleado",emp);
        RequerimientoDTO req = new RequerimientoDTO();
        model.addAttribute("requerimiento", req);
        return "requerimiento";
    }
    @PostMapping("/sendRequerimiento")
    public String sendEmail(HttpSession session,Model model,@ModelAttribute("requerimiento") RequerimientoDTO req) {
        EmpleadoDTO emp = (EmpleadoDTO) session.getAttribute("empleado");
        if (emp == null) {
            return "redirect:/login";
        }
        if (req == null) {
            return "redirect:/login";
        }
        List<EmpleadoDTO> analistas = empleadoService.findByTipoCargo("age");
        model.addAttribute("fase","fase1");
        model.addAttribute("empleado",emp);
        model.addAttribute("analistas", analistas);
        session.setAttribute("requerimiento", req);
        return "seleccionarAnalista";
    }
    
    @PostMapping("/finFase1")
    public String postMethodName(@RequestParam String analistaId,HttpSession session) {
        EmpleadoDTO emp = (EmpleadoDTO) session.getAttribute("empleado");
        if (emp == null) {
            return "redirect:/login";
        }
        RequerimientoDTO req = (RequerimientoDTO) session.getAttribute("requerimiento");
        if (req == null) {
            return "redirect:/login";
        }
        req.setFechaReque(LocalDate.now());
        req.setIdResponsable(emp.getCodEmpleado());
        req.setIdEmplAsignado(analistaId);
        System.out.println(req);
        requerimientoService.saveRequerimiento(req,"1");
        session.invalidate();
        return "redirect:/login";
    }
    
    @GetMapping("/fase-2")
    public String fase2Init(HttpSession session,Model model) {
        EmpleadoDTO emp = (EmpleadoDTO) session.getAttribute("empleado");
        if (emp == null) {
            return "redirect:/login";
        }
        model.addAttribute("fase","fase2");
        model.addAttribute("empleado",emp);
        List<RequerimientoDTO> requerimientos = requerimientoService.finByCodEmpleado(emp.getCodEmpleado());
        System.out.println(requerimientos);
        model.addAttribute("requerimientos",requerimientos);
        session.setAttribute("requerimientos", requerimientos);
        return "fase2";
    }

    @GetMapping("path")
    public String showReque(HttpSession session) {
        return new String();
    }
    
    

}
