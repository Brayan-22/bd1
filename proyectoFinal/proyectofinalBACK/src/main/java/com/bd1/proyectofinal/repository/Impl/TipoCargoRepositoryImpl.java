package com.bd1.proyectofinal.repository.Impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bd1.proyectofinal.models.TipoCargoDTO;
import com.bd1.proyectofinal.repository.TipoCargoRepository;


@Repository
public class TipoCargoRepositoryImpl implements TipoCargoRepository{

    private final JdbcTemplate jdbcTemplate;

    public TipoCargoRepositoryImpl(@Autowired JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    private RowMapper<TipoCargoDTO> rowMapper = (rs,rowNum) ->{
        return new TipoCargoDTO(rs.getString("idtipocargo"),rs.getString("desctipocargo"));
    };
    @Override
    public List<TipoCargoDTO> getAll() {
        String sql = "select * from tipocargo";
        return jdbcTemplate.query(sql, rowMapper);
    }
    
}
