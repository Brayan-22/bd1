package com.bd1.proyectofinal.repository;

import java.util.List;

import com.bd1.proyectofinal.models.RequerimientoDTO;

public interface RequerimientoRepository {
    public void saveRequerimiento(RequerimientoDTO req,String fase);
    public List<RequerimientoDTO> findByCodEmpleado(String codEmpleado);
}
