package com.bd1.proyectofinal.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TipoCargoDTO {
    private String idTipoCargo;
    private String descTipoCargo;
}
