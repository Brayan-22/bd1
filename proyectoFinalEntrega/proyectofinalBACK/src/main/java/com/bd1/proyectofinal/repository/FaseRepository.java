package com.bd1.proyectofinal.repository;


import java.util.*;

import com.bd1.proyectofinal.models.FaseDTO;
public interface FaseRepository {
    public List<FaseDTO> getAllFaseByCargo(String idCargo);
}
