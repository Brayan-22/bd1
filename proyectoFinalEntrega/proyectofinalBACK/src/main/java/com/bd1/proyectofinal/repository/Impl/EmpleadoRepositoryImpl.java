package com.bd1.proyectofinal.repository.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bd1.proyectofinal.models.EmpleadoDTO;
import com.bd1.proyectofinal.models.TipoCargoDTO;
import com.bd1.proyectofinal.repository.EmpleadoRepository;


@Repository
public class EmpleadoRepositoryImpl implements EmpleadoRepository{

    private final JdbcTemplate plantilla;

    public EmpleadoRepositoryImpl(@Autowired JdbcTemplate plantilla){
        this.plantilla = plantilla;
    }
    private RowMapper<EmpleadoDTO> rowMapper = (rs,rowNum) ->{
        return new EmpleadoDTO(rs.getString("CODEMPLEADO"), rs.getString("IDTIPOCARGO"),rs.getString("NOMEMPLEADO"),rs.getString("DESCTIPOCARGO"));
    };

    @Override
    public EmpleadoDTO findByCorreo(String correo) {
        String sql = "select E.NOMEMPLEADO,E.CODEMPLEADO,C.IDTIPOCARGO,TC.DESCTIPOCARGO from empleado E,cargo C,tipocargo TC where E.correo=? and E.CODEMPLEADO=C.CODEMPLEADO and TC.IDTIPOCARGO=C.IDTIPOCARGO";
        return plantilla.queryForObject(sql, rowMapper, correo);
    }

    @Override
    public List<EmpleadoDTO> findByTipoCargo(String idTipoCargo) {
        String sql = "select E.NOMEMPLEADO,E.CODEMPLEADO,C.IDTIPOCARGO,tc.DESCTIPOCARGO from empleado E,cargo C, tipoCargo tc where E.codEmpleado=C.codEmpleado and C.IDTIPOCARGO=? and C.IDTIPOCARGO=tc.IDTIPOCARGO";
        return plantilla.query(sql, rowMapper, idTipoCargo);
    }
    
}
