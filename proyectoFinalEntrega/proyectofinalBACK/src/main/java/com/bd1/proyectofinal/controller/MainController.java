package com.bd1.proyectofinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bd1.proyectofinal.models.EmpleadoDTO;
import com.bd1.proyectofinal.services.EmpleadoService;

import jakarta.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;




@Controller
@RequestMapping(path = "/")
public class MainController {


    @Autowired
    private EmpleadoService empleadoService;
    
    @GetMapping
    public String intit() {
        return "index";
    }
    @GetMapping("/login")
    public String login(){
        return "login";
    }
    
    @PostMapping("/login")
    public String procesarLogin(@RequestParam String correo,HttpSession session,Model model) {
        EmpleadoDTO empleado = empleadoService.findByCorreo(correo);
        if (empleado!=null) {
            session.setAttribute("empleado", empleado);
            return "redirect:/seleccionarFase";
        }else{
            model.addAttribute("error","Empleado no encontrado");
            return "login";
        }
    }

    @GetMapping("/logout")
    public String logOut(HttpSession session) {
        session.invalidate();
        return "redirect:/login";
    }
    
    


}
