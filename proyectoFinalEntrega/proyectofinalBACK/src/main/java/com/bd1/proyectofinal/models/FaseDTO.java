package com.bd1.proyectofinal.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FaseDTO {
    private String idFase;
    private String descFase;
}
