package com.bd1.proyectofinal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bd1.proyectofinal.models.EmpleadoDTO;
import com.bd1.proyectofinal.repository.EmpleadoRepository;


import java.util.*;
@Service
public class EmpleadoService {

    @Autowired
    private EmpleadoRepository empleadoRepository;


    public EmpleadoDTO findByCorreo(String correo){
        return empleadoRepository.findByCorreo(correo);
    } 

    public List<EmpleadoDTO> findByTipoCargo(String idTipoCargo){
        return empleadoRepository.findByTipoCargo(idTipoCargo);
    }
}
