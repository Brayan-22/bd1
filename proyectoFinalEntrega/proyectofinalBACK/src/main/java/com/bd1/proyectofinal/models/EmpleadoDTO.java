package com.bd1.proyectofinal.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmpleadoDTO {
    private String codEmpleado;
    private String idTipoCargo;
    private String nombre;
    private String descCargo;
}
