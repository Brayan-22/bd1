package com.bd1.proyectofinal.repository;

import java.util.*;

import com.bd1.proyectofinal.models.TipoCargoDTO;

public interface TipoCargoRepository {
    List<TipoCargoDTO> getAll();
}
