--1.seleccione el nombre y apellido de todos los empleados
-- que tienen el mismo cargo que carmen velásquez



SELECT E.FIRST_NAME || ' ' || E.LAST_NAME empleado
FROM S_EMP E WHERE E.TITLE IN (SELECT E.title
FROM S_EMP E WHERE LOWER(E.FIRST_NAME) LIKE 'carmen'
AND LOWER(E.LAST_NAME) LIKE 'velasquez') 
AND lower(E.FIRST_NAME) NOT LIKE 'carmen' 
AND lower(E.LAST_NAME) NOT LIKE 'velasques';

--2.Liste el apellido, el cargo y el id del depto de todos
--los empleados que trabajan en el mismo depto que Colin

SELECT E.LAST_NAME,E.TITLE ,E.DEPT_ID
FROM S_EMP E WHERE E.DEPT_ID IN (SELECT E.DEPT_ID 
FROM S_EMP E WHERE LOWER(E.FIRST_NAME) LIKE 'colin');

--3.Liste los empleados que ganan el maximo salario

SELECT E.FIRST_NAME || ' ' || E.LAST_NAME empleados
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID 
AND S.PAYMENT IN (SELECT MAX(S.PAYMENT)
FROM S_SALARY S);

--SELECT E.FIRST_NAME || ' ' ||E.LAST_NAME empleados,MAX(S.PAYMENT) maximoSalario 
--FROM S_EMP E,S_SALARY S 
--WHERE E.ID = S.ID
--GROUP BY E.FIRST_NAME,E.LAST_NAME;



--4.Liste los empleados cuyo salario es al menos como el promedio de salario


SELECT DISTINCT E.FIRST_NAME || ' '|| E.LAST_NAME empleados
FROM S_SALARY S,S_EMP E
WHERE S.ID = E.ID AND 
s.PAYMENT >= (SELECT AVG(S.PAYMENT) promedio
FROM S_SALARY S);



--5. Liste los departamentos cuyo promedio de salario es superior al promedio general


SELECT D.NAME,AVG(s.PAYMENT) promedioSalario 
FROM S_EMP E,S_SALARY S,S_DEPT D
WHERE E.ID = S.ID 
AND E.DEPT_ID = D.ID
HAVING AVG(S.PAYMENT)>(SELECT AVG(S.PAYMENT) promedio
FROM S_SALARY S)
GROUP BY D.NAME;


--6. Liste los empleados que ganan el maximo salario por departamento

SELECT DISTINCT E.DEPT_ID , E.FIRST_NAME || ' ' || E.LAST_NAME empleados,S.PAYMENT salario
FROM S_EMP E,S_SALARY S,
(
	SELECT D.ID deptID,max(s.PAYMENT) maxSalario 
	FROM S_EMP E,S_SALARY S,S_DEPT D
	WHERE E.ID = S.ID 
	AND E.DEPT_ID = D.ID
	GROUP BY D.ID
)maxSalDept
WHERE E.ID = S.ID 
AND E.DEPT_ID = maxSalDept.deptID
AND S.PAYMENT = maxSalDept.maxSalario;


--7. Liste el ID, el apellido y el nombre del departamento de todos
--los empleados que trabajan en un departamento que tengan al menos
--un empleado de apellido PATEL

SELECT distinct E.ID,E.LAST_NAME,D.NAME dpto
FROM S_EMP E,S_DEPT D
WHERE E.DEPT_ID = D.ID
AND E.DEPT_ID IN (SELECT DISTINCT  D.ID deptID
FROM S_EMP E,S_DEPT D,S_REGION R
WHERE E.DEPT_ID = D.ID 
AND D.REGION_ID = R.ID 
AND LOWER(E.LAST_NAME) LIKE 'patel');



--8. Liste el ID, el apellido y la fecha de entrada de todos los empleados
--cuyos salarios son menores que el promedio general de salario y trabajan
-- en algun departamento que cuente con un empleado de apellido PATEL

SELECT DISTINCT E.ID,E.LAST_NAME,TO_CHAR(E.START_DATE ,'YYYY/MM/DD')
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID
AND S.PAYMENT<(SELECT AVG(S.PAYMENT) promedioGen
FROM S_SALARY S)
AND E.DEPT_ID IN (SELECT DISTINCT  D.ID deptID
FROM S_EMP E,S_DEPT D,S_REGION R
WHERE E.DEPT_ID = D.ID 
AND D.REGION_ID = R.ID 
AND LOWER(E.LAST_NAME) LIKE 'patel');

--9.Liste el id del cliente,el nomnbre y el record de ventas de todos los clientes
--que están localizados en North America o tienen a Magge como representante de ventas.
--Trabajar todo el ejercicio con select anidados


SELECT MAX(cantVentas.ventas) 
FROM (
	SELECT C.ID idCliente,COUNT(C.NAME) ventas
	FROM S_CUSTOMER C JOIN S_ORD O ON C.ID = O.CUSTOMER_ID
	GROUP BY C.ID 
)cantVentas
GROUP BY 

SELECT DISTINCT C.ID idCliente
FROM S_CUSTOMER C,S_REGION R
WHERE C.REGION_ID = R.ID 
AND LOWER(R.NAME) LIKE 'north%' 
UNION
SELECT C.ID idCliente
FROM S_CUSTOMER C,S_EMP E
WHERE C.SALES_REP_ID = E.ID ;



SELECT * FROM S_CUSTOMER C;

--10.Liste los empleados que ganan en promedio más que el promedio de salario
--de su departamento(siempre que se hable de departamento se debe tener la region, ya que
--los departamentos tienen igual nombre, pero son diferentes)

SELECT DISTINCT E.DEPT_ID ,E.REGION_ID,E.FIRST_NAME || ' '|| E.LAST_NAME empleados, promedioEmpleado.promEmp,promDept.promDept
FROM S_EMP E,S_SALARY S,
(SELECT E.ID empID,AVG(S.PAYMENT) promEmp
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID 
GROUP BY E.ID)promedioEmpleado,
(
	SELECT D.ID dpto ,R.ID reg ,AVG(S.PAYMENT) promDept
	FROM S_EMP E,S_SALARY s,S_DEPT D,S_REGION R
	WHERE E.ID = S.ID 
	AND E.DEPT_ID = D.ID 
	AND E.REGION_ID = R.ID 
	GROUP BY  D.ID ,R.ID
)promDept
WHERE E.ID = S.ID
AND E.DEPT_ID = promDept.dpto
AND E.REGION_ID = promDept.reg
AND E.ID = promedioEmpleado.empID
AND promedioEmpleado.promEmp > promDept.promDept;

--11. Listar los empleados a cargo de los vicepresidentes
SELECT E.LAST_NAME empleado,E.TITLE tituloEmpleado,Man.LAST_NAME jefe,Man.TITLE tituloJefe
FROM S_EMP E,S_EMP Man 
WHERE E.MANAGER_ID = MAN .ID 
AND LOWER(E.TITLE)  LIKE 'vp%';

--12. Listar los empleados que trabajan en el mismo departamento que Ngao


SELECT E.FIRST_NAME || ' ' ||E.LAST_NAME empleados
FROM S_EMP E
WHERE E.DEPT_ID IN (SELECT DISTINCT E.DEPT_ID
FROM S_EMP E,S_DEPT D
WHERE E.DEPT_ID = D.ID 
AND LOWER(E.LAST_NAME) LIKE 'ngao');

--13.Liste el promedio de salario de todos los empleados que tienen el mismo cargo que Havel

SELECT E.FIRST_NAME ,E.LAST_NAME,AVG(S.PAYMENT)
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID 
AND LOWER(E.TITLE) LIKE (SELECT LOWER(E.TITLE)
FROM S_EMP E
WHERE LOWER(E.LAST_NAME) LIKE 'havel')
AND LOWER(E.LAST_NAME)  NOT LIKE 'havel'
GROUP BY E.FIRST_NAME ,E.LAST_NAME;

--14.Cuantos empleados ganan igual que Giljum Henry 

SELECT count(*)
FROM (
	SELECT E.LAST_NAME ,SUM(S.PAYMENT) sueldoGil
	FROM S_EMP E,S_SALARY S
	WHERE E.ID = S.ID 
	AND E.LAST_NAME LIKE 'Giljum'
	GROUP BY E.LAST_NAME
)giljum,
(
	SELECT E.LAST_NAME ,SUM(S.PAYMENT) sueldoNoGil
	FROM S_EMP E,S_SALARY S
	WHERE E.ID = S.ID 
	AND E.LAST_NAME NOT LIKE 'Giljum'
	GROUP BY E.LAST_NAME
)noGiljum
WHERE giljum.sueldoGil = noGiljum.sueldoNoGil;



--15.Liste todos los empleados que no están a cargo de un administrador de bodega



--16.Calcule el promedio de salario por departamento de todos los empleados
--que ingresaron a la compañia en el mismo año que Smith George


SELECT E.DEPT_ID ,AVG(S.PAYMENT) promSalario
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID 
AND EXTRACT(YEAR FROM E.START_DATE) IN (
	SELECT EXTRACT(YEAR FROM E.START_DATE)
	FROM S_EMP E
	WHERE E.LAST_NAME LIKE 'Smith'
	AND E.FIRST_NAME LIKE 'George'
)
GROUP BY E.DEPT_ID ;



--17. Liste el promedio de ventas de los empleados que están a cargo de los
--vicepresidentes comerciales

SELECT E.LAST_NAME empleado,E.TITLE cargoEmpleado,vendedores.promVentas
FROM S_EMP E,S_EMP Man,(
	SELECT E.ID vendedor,AVG(O.TOTAL) promVentas
	FROM S_EMP E,S_ORD O
	WHERE E.ID = O.SALES_REP_ID 
	GROUP BY E.ID 
)vendedores
WHERE E.MANAGER_ID = Man.ID 
AND E.ID = vendedores.vendedor
AND Man.TITLE LIKE 'VP, Sales';


--18. Liste el salario mensual de cada uno de los empleados teniendo en 
--cuenta la comision por venta

