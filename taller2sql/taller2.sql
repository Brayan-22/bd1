-- 1.Liste el apellido, el id del departamento y el nombre 
--del departamento de todos los empleados

SELECT DISTINCT LAST_NAME apellido, D.ID idDepartamento,D.NAME nombreDepartamento
FROM S_EMP E,S_DEPT D
WHERE D.ID = E.DEPT_ID  ;


--2.Liste el id del departamento,el id de la region y el nombre de la region de todos los departamentos
SELECT D.ID id_Departamento,R.ID id_Region,R.NAME region
FROM S_DEPT D,S_REGION R
WHERE D.REGION_ID = R.ID ;

-- 3.liste el apellido, el id del departamento y el nombre del departamento de
-- los empleados con apellido 'Menchu'

SELECT DISTINCT LAST_NAME apellido, D.ID idDepartamento,D.NAME nombreDepartamento
FROM S_EMP E,S_DEPT D
WHERE D.ID = E.DEPT_ID
AND E.LAST_NAME = 'Menchu';

-- 4.Liste el id de la region, el nombre de la region, el id del departamento
-- y el nombre del departamento de todos los departamentos que pertenecen a la region
-- 'North America'

SELECT R.ID regionId,R.NAME region,D.ID departId,D.NAME departamento
FROM S_DEPT D,S_REGION R
WHERE D.REGION_ID = R.ID 
AND R.NAME = 'North America';

--5. Liste los nombres de los representantes de ventas. Y, de todos los clientes el Id y los nombres

SELECT E.FIRST_NAME || ' ' || E.LAST_NAME representanteVentas,C.ID id_cliente,C.NAME nombre_cliente
FROM S_EMP E,S_CUSTOMER C
WHERE C.SALES_REP_ID = E.ID ;

-- 6. List el id de los clientes, los nombres de los clientes y el id de las ordenes. De todos los clientes
-- con o sin ordentes

SELECT C.ID id_cliente, C.NAME cliente, NVL(TO_CHAR(O.ID),'sin orden') idOrden 
FROM S_CUSTOMER C LEFT JOIN S_ORD O ON C.ID = O.CUSTOMER_ID ;

-- 7. Seleccionar los nombres de los productos con su fecha de recargue
SELECT P.NAME nombre_Producto, TO_CHAR(I.RESTOCK_DATE,'DD/MM/YYYY')AS fechaRecargue
FROM S_PRODUCT P,S_INVENTORY I
WHERE P.ID = I.PRODUCT_ID;

-- 8. Seleccionar los empleados con sus departamentos y regiones
SELECT E.FIRST_NAME || ' ' || E.LAST_NAME empleados, D.NAME ,R.NAME 
FROM S_EMP E,S_REGION R,S_DEPT D
WHERE E.DEPT_ID = D.ID 
AND D.REGION_ID = R.ID ;

-- 9. Seleccionar los productos con sus regiones
SELECT P.NAME producto,R.NAME region
FROM S_PRODUCT P,S_INVENTORY I,S_WAREHOUSE W,S_REGION R
WHERE P.ID = I.PRODUCT_ID 
AND I.WAREHOUSE_ID = W.ID 
AND W.REGION_ID = R.ID ;


-- 10. Seleccionar los representantes de ventas que han hecho ordenes de items
-- con mas de 500 unidades
SELECT E.FIRST_NAME || ' ' || E.LAST_NAME representante_ventas, O.ID orden, I.QUANTITY items
FROM S_ORD O,S_EMP E,S_ITEM I
WHERE O.SALES_REP_ID = E.ID 
AND I.ORD_ID = O.ID 
AND I.QUANTITY > 500;

-- 11. Seleccionar los clientes y empleados que trabajan en la misma region

SELECT E.FIRST_NAME || ' ' || E.LAST_NAME empleados,C.NAME cliente,R.NAME region
FROM S_EMP E,S_CUSTOMER C,S_REGION R
WHERE C.SALES_REP_ID = E.ID
AND C.REGION_ID = E.REGION_ID
AND C.REGION_ID = R.ID ;

-- 12. Seleccionar los productos que se han vendido y cuentan con un stock de 
-- mas de 130 unidades. ayuda: utilizar el distinct(product.name)

SELECT DISTINCT(P.NAME) producto, I.AMOUNT_IN_STOCK stock
FROM S_PRODUCT P,S_ORD O,S_ITEM It,S_INVENTORY I
WHERE O.ID = It.ORD_ID 
AND It.PRODUCT_ID = P.ID 
AND I.PRODUCT_ID = P.ID 
AND I.AMOUNT_IN_STOCK > 130;


-- 13. Seleccionar los nombres completos de los representantes de ventas
SELECT E.FIRST_NAME || ' ' || E.LAST_NAME empleados
FROM S_EMP E
WHERE TITLE LIKE 'Sales%';

-- 14. Seleccionar los nombres completos de los empleados que no son representantes de ventas
SELECT E.FIRST_NAME || ' ' || E.LAST_NAME empleados
FROM S_EMP E
WHERE TITLE NOT LIKE 'Sales%';


--15. Seleccionar los nombres completos y cargo tanto de los empleados con sus subalternos
-- (debe listar empleado y subalterno)

SELECT E.FIRST_NAME || ' ' || E.LAST_NAME jefe,E.TITLE cargo_jefe,Sub.FIRST_NAME || ' ' || Sub.LAST_NAME subalterno,Sub.TITLE cargoSubalterno
FROM S_EMP E LEFT JOIN S_EMP Sub ON Sub.MANAGER_ID = E.ID ;


--16. Seleccionar los nombres completos y cargo de los empleados que tienen subalternos (debe listar solo los empleados)

SELECT E.FIRST_NAME || ' ' || E.LAST_NAME jefe,E.TITLE cargo_jefe,Sub.FIRST_NAME || ' ' || Sub.LAST_NAME subalterno,Sub.TITLE cargoSubalterno
FROM S_EMP E INNER JOIN S_EMP Sub ON Sub.MANAGER_ID = E.ID ;

--17. Seleccionar los nombres completos y cargo de todos los empleados con sus subalternos tengan o no tengan subalterno

SELECT E.FIRST_NAME || ' ' || E.LAST_NAME jefe,E.TITLE cargo_jefe,Sub.FIRST_NAME || ' ' || Sub.LAST_NAME subalterno,Sub.TITLE cargoSubalterno
FROM S_EMP E LEFT JOIN S_EMP Sub ON Sub.MANAGER_ID = E.ID ;

--18. Seleccionar los nombres completos y cargos de todos los empleados que no tienen subalternos
select J.LAST_NAME || ' ' || J.FIRST_NAME AS Empleado,
J.TITLE AS CARGO_jefe,
E.LAST_NAME || ' ' || E.FIRST_NAME AS Subalterno
FROM s_emp J left JOIN s_emp E
on J.ID = E.MANAGER_ID
where E.MANAGER_ID IS NULL;

--19. Seleccionar los nombres completos y cargo de los subalternos que no tienen fejes
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,E.TITLE AS cargo,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE 
FROM s_emp J right JOIN s_emp E
on J.ID = E.MANAGER_ID
WHERE E.MANAGER_ID IS NULL;

--20. Seleccionar los productoss que no están en el inventario
SELECT DISTINCT P.NAME 
FROM S_PRODUCT P,S_INVENTORY I
WHERE P.ID =I.PRODUCT_ID 
MINUS
SELECT P.NAME
FROM S_PRODUCT P;

--21. Seleccionar los productos que nunca se han pedido
SELECT DISTINCT P.NAME 
FROM S_PRODUCT P
MINUS
SELECT DISTINCT P.NAME 
FROM S_ORD O, S_ITEM I,S_PRODUCT P
WHERE O.ID = I.ORD_ID 
AND I.PRODUCT_ID = P.ID ;

--22. Seleccionar las regiones que no tienen productos en el inventario

SELECT R.NAME ,I.PRODUCT_ID, I.AMOUNT_IN_STOCK  
FROM S_INVENTORY I,S_WAREHOUSE W,S_REGION R
WHERE W.ID = I.WAREHOUSE_ID 
AND W.REGION_ID = R.ID 
AND I.AMOUNT_IN_STOCK = 0;

-- 23. Seleccionar el salario total y promedio de cada uno de los empleados

SELECT E.FIRST_NAME,SUM(s.PAYMENT) total,AVG(S.PAYMENT) promedio 
FROM S_EMP E,S_SALARY S
WHERE E.ID = S.ID 
GROUP BY E.FIRST_NAME ;


--24. Generar un listado de los empleados indicando si ganan o no comision

SELECT E.FIRST_NAME ||' '||E.LAST_NAME empleado,NVL2(E.COMMISSION_PCT,'Gana comision','No gana comision') comision
FROM S_EMP E;