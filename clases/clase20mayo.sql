-- orden de consultas agrupadas

select
from -- MEZCLA DE COMPARACIONES ENTRE DATOS AGRUPADOS Y NO AGRUPADOS
where {atributo simple} operador {valores}
group by 
having {funcion grupo(atributo)} {operador(valores)}


-- operadores que incluyen varias filas
In
not In
any --> >any, >=any
some
all

-- seleccionar los clientes que han comprado en promedio mas que la 
-- mitad de las compras del promedio general de compras

select C.name clientes
from s_customer C
join s_ord O on C.id=O.customer_id
having
    avg(O.TOTAL) > (
        select avg(O.TOTAL)/2 from s_ord O
    )
group by C.NAME;

select avg(o.total)/2 from s_ord O;

--seleccionar los clientes que han comprado en total
--tanto como el total comprado por alguna region


select C.name, sum(O.total)
from s_customer C,s_ord O
where C.id = O.customer_id
having sum(O.total) in (select sum(O.total)
from s_ord O,s_customer C,s_region R
where O.customer_id = C.id
and C.region_id = R.id
group by R.id)
group by C.name;

--total en cada region
select sum(O.total)
from s_ord O,s_customer C,s_region R
where O.customer_id = C.id
and C.region_id = R.id
group by R.id;


-- selecionar los clientes que han comprado
-- en total mas que el total comprado por alguna region


select C.name, sum(O.total)
from s_customer C,s_ord O
where O.customer_id=C.id
having  sum(O.total) >= any (
    select sum(O.total)
    from s_ord O,s_customer C,s_region R
    where o.customer_id=C.id and C.region_id=R.id
    group by R.id
)
group by C.name;


--seleccionar los empleados que en promedio por salario les han pagado
-- mas que lo todos los promedios pagados por region



select E.first_name || ' ' || E.last_name,avg(S.payment) prom_salario
from s_emp E,s_salary S
where E.id = S.id
group by E.first_name || ' ' || E.last_name;




select C.name,S.payment from s_emp E,s_salary S
where E.id = S.id;

select E.firs_name,(avg(S.payment)) from s_emp E,s_salary S
where E.id = S.id
group by E.first_name;



--SELECCIONAR LOS DEPARTAMENTOS CUYO SALARIO PROMEDIO ES MAYOR QUE EL SALARIO PROMEDIO DE SU REGION



--seleccionar los compañeros de patel que en promedio ganan mas que el promedio de patel

select E.first_name ||' '|| E.last_name empleado 
from s_emp E join s_salary S on E.id=S.id 
where 

---23 compañeros

select distinct D.name 
from s_dept D,
(select E.region_id regD,E.dept_id DepD,avg(S.payment) pagD
    from s_dept D,s_emp E,s_salary S
    where D.id = E.dept_id and D.region_id = E.region_id and
    E.id = S.id
    group by E.region_id,E.dept_id) Depto,
(select E.region_id regR, avg(S.payment) pagR
    from s_dept D,s_emp E,s_salary S
    where D.id = E.dept_id and D.region_id = E.region_id and
    E.id = S.id
    group by E.region_id
) Region
where Depto.regD = Region.regR and
    Depto.pagD > Region.pagR and
    D.id = Depto.DepD;



-- seleccionar los compañeros de patel que en promedio ganan mas
-- que el promedio de patel



select E.first_name || ' ' || E.last_name compañeros,noPa.ProNoPa
from s_emp E,
(select E.region_id rePa,E.dept_id DePa,E.id,avg(S.payment) ProPa
    from s_emp E,s_salary S
    where E.id = S.id and 
    lower(E.last_name) like 'patel'
    group by E.region_id,E.dept_id,E.id
)Pa,
(select E.region_id ReNoPa,E.dept_id DeNoPa,E.id codNoPa,avg(S.payment)ProNoPa
    from s_emp E,s_salary S
    where E.id = S.id and
    lower(E.last_name) not like 'patel'
    group by E.region_id,E.dept_id,E.id
)noPa
where Pa.rePa = noPa.ReNoPa and
    Pa.DePa = noPa.DeNoPa and
    Pa.ProPa < noPa.ProNoPa and 
    E.id = noPa.codNoPa;



--seleccionar los empleados con:
--Empleado,año,mes,salario,comision,totalPagado

--Empleados que no ganan comision


select E.first_name||' '||E.last_name Employee,
extract(YEAR FROM S.DATEPAYMENT)YEAR,
extract(MONTH FROM S.DATEPAYMENT)MONTH, S.payment,
nvl(E.commission_pct,0)commission,S.payment total
from s_emp E join s_salary S on S.id = E.id
where E.Commission_pct is null;


--Empleados que ganan comision y no tienen ordenes


SELECT EMP.employee,EMP.YEAR,EMP.MONTH,EMP.TOTAL,
        ROUND(ORD.TOT*EMP.COMMISSION/100,2) COMMISSION,
        EMP.TOTAL+ROUND(ORD.TOT*EMP.COMMISSION/100,2)TOTAL
from
    (select E.first_name||' '||E.last_name employee,
    extract(YEAR FROM S.DATEPAYMENT) YEAR,
    extract(MONTH FROM S.DATEPAYMENT) MONTH,
    NVL(E.COMMISSION_PCT,0) COMMISSION,S.PAYMENT TOTAL
    from s_emp E join s_salary S on E.id=S.id
    where E.Commission_pct is not null
    )EMP,
    (select E.first_name ||' '||E.last_name employee,
        extract(YEAR FROM O.DATE_ORDERED) YEAR,
        extract(MONTH FROM O.DATE_ORDERED) MONTH,
    sum(O.TOTAL) TOT
    from s_emp E,s_ord O
    where E.id = O.sales_rep_id
    group by E.first_name,E.last_name,
        extract(YEAR FROM O.DATE_ORDERED),
        extract(MONTH FROM O.DATE_ORDERED)
    )ORD
    where EMP.employee = ORD.employee and
        EMP.YEAR = ORD.YEAR AND
        EMP.MONTH = ORD.MONTH
UNION 
SELECT E.first_name||' '||E.last_name employee,
    extract(YEAR FROM S.DATEPAYMENT) YEAR,
    extract(MONTH FROM S.DATEPAYMENT) MONTH,S.PAYMENT,
    NVL(E.COMMISSION_PCT,0) COMMISSION,S.PAYMENT TOTAL
    FROM S_EMP E
    JOIN s_salary S on S.id = E.id
    where E.COMMISSION_PCT IS NOT NULL AND
    (E.FIRST_NAME,E.LAST_NAME,extract(YEAR FROM S.DATEPAYMENT),
    extract(MONTH FROM S.DATEPAYMENT)) NOT IN (
        select E.first_name,E.last_name,
        extract(YEAR FROM O.DATE_ORDERED),
        extract(MONTH FROM O.DATE_ORDERED)
        from s_emp E,s_ord O
        where E.id = O.sales_rep_id
    );










select E.region_id,E.dept_id,avg(S.payment)
from s_emp E,s_salary S
where E.id = S.id and lower(E.last_name) = 'patel'
group by E.region_id,E.dept_id;




select E.dept_id, max(S.payment)
from s_emp E,s_salary S,s_dept D
where E.id = S.id
and E.dept_id = D.id
group by E.dept_id;