--seleccionar las regiones que tienen representantes de ventas pero no tienen vicepresidentes

select DISTINCT R.name 
from s_region R JOIN s_emp E 
on R.id = E.region_id
where lower(E.title) like 'sales representative%'
MINUS
select DISTINCT R.name 
from s_region R JOIN s_emp E 
on R.id = E.region_id
where lower(E.title) like 'vp%';


-- seleccionar las ordenes (id) que fueron ordenadas en el mes de agosto , los nombres completos de los representantes que ordenaron
-- y el nombre completo de los vendedores que despacharon


select O.id ordenId, O.date_ordered,
R.first_name || ' ' || R.last_name representante,
D.first_name || ' ' || D.last_name despachador
from s_emp R,s_emp D,s_ord O
where R.id = O.sales_rep_id and
      D.id = O.stock_clerk and
      extract(MONTH FROM O.date_ordered) = 8;