--BASICAS
select first_name, lpad(salary,20,'*') as salario
  from s_emp;


select first_name, rpad(salary,20,'*') as salario
from s_emp;

select first_name, translate(first_name,'aeiou','1234') as cambio
from s_emp;

select first_name, replace(title,'Sales','ventas') as cambio
from s_emp;

select first_name, salary, commission_pct comision from s_emp;

select first_name,salary+((commission_pct/100)*salary) as sueldo
from s_emp;

select first_name, salary+((NVL(commission_pct,0)/100)*salary) as sueldo from s_emp;

update s_emp set commission_pct = NVL(commission_pct,0);

--BASICAS 2
select title,((NVL(commission_pct,0)/100)*salary) as sueldo,salary,commission_pct
from s_emp;

select title,round(((NVL(commission_pct,0)/100)*salary)) as sueldo,salary,commission_pct
from s_emp;

select title,trunc(((NVL(commission_pct,0)/100)*salary)) as sueldo,salary,commission_pct
from s_emp;

select first_name,to_char(start_date,'Day DD "de" Month "de" YYYY')as fechaInicio from
