--seleccionar los empleados que les han pagado
--tanto como el maximo de su region


select distinct E.first_name || ' ' || E.last_name empleados
from s_emp E,s_salary S
where E.id = S.id and
(E.region_id,S.payment) in (Select E.region_id,max(S.payment)
from s_emp E,s_salary S
where E.id = S.id
group by E.region_id);

--seleccionar los empleados compañeros de patel


select distinct E.first_name|| ' ' ||
E.last_name empleados,R.name,D.name
from s_emp E,s_region R,s_dept D
where E.region_id = R.id
and E.dept_id = D.id
and (E.region_id,E.dept_id) in (select E.region_id,E.dept_id
from s_emp E
where lower(last_name) ='patel') and E.last_name not like 'Patel';


select 
from
where
group by
having (funcion de agrupamiento(atributo) operador valor)


--seleccionar los empleados que en promedio han ganado mas que el promedio general


select E.first_name||' '||E.last_name empleados,avg(S.payment)
from s_emp E,s_salary S
where E.id = S.id
group by E.first_name,E.last_name
having avg(S.payment) > (select avg(S.payment)
from s_salary S);

--seleccionar los empleados que en promedio han ganado tanto como el promedio de su depto

select E.first_name||' '||E.last_name empleados,avg(S.payment)
from s_emp E,s_salary S,s_dept D,s_region R
where E.id = S.id
and E.dept_id = D.id 
and E.region_id = R.id



group by E.first_name,E.last_name;



select D.id,D.name,avg(S.payment)
from s_emp E,S_salary S,s_dept D,s_region R
where E.id = S.id 
and E.dept_id = D.id
and E.region_id=R.id
group by D.id,D.name;


