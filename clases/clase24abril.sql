-- seleccionar las regiones que hicieron ordenes en el mes de agosto
select O.ID idOrden,C.ID idCustomer,C.REGION_ID idRegion,R.NAME nombreRegion,O.DATE_ORDERED 
from s_ord O JOIN s_customer C on O.CUSTOMER_ID=C.ID 
JOIN s_region R on C.REGION_ID=R.ID 
where extract(month from O.DATE_ORDERED)=8;


---seleccionar los nombres de los empleados con los nombres de los jefes

select emp.FIRST_NAME || ' '|| emp.LAST_NAME AS empleado,emp.title as cargoEmp,jefe.FIRST_NAME ||' ' || jefe.LAS
T_NAME as jefe,jefe.title as cargoJefe
from s_emp emp JOIN s_emp jefe
on emp.MANAGER_ID=jefe.ID
order by jefe.FIRST_NAME;


--seleccionar los nombres de los empleados tengan o no jefe

-- opcion 1 :
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE 
FROM s_emp E JOIN s_emp J 
on J.ID (+)= E.MANAGER_ID;

--opcion 2:
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE 
FROM s_emp J right JOIN s_emp E
on J.ID = E.MANAGER_ID;

--opcion 3:
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE 
FROM s_emp E left JOIN s_emp J
on J.ID = E.MANAGER_ID;


--- seleccionar todos los jefes tengan o no tenga subalternos
--opcion 1
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE , J.TITLE as CARGO
FROM s_emp E JOIN s_emp J 
on J.ID = E.MANAGER_ID (+);

--opcion 2
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE,
J.TITLE AS CARGO
FROM s_emp J left JOIN s_emp E
on J.ID = E.MANAGER_ID;

--opcion 3 
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE,
J.TITLE AS CARGO
FROM s_emp E right JOIN s_emp J
on J.ID = E.MANAGER_ID;


--seleccionar todos los empleados que no tengan jefe
--opcion 1
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE 
FROM s_emp J right JOIN s_emp E
on J.ID = E.MANAGER_ID
WHERE E.MANAGER_ID IS NULL;
-- opcion 2
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE 
FROM s_emp E left JOIN s_emp J
on J.ID = E.MANAGER_ID
where E.MANAGER_ID IS NULL;


--seleccionar los empleados que no tienen subalternos

--opcion 1:
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE,
J.TITLE AS CARGO
FROM s_emp J left JOIN s_emp E
on J.ID = E.MANAGER_ID
where E.MANAGER_ID IS NULL;
--OPCION 2:
select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE,
J.TITLE AS CARGO
FROM s_emp E right JOIN s_emp J
on J.ID = E.MANAGER_ID
where E.MANAGER_ID IS NULL;


--seleccionar a todos los empleados tengan jefe o no jefe o subalternos

select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE,
J.TITLE AS CARGO
FROM s_emp J FULL OUTER JOIN s_emp E
on J.ID = E.MANAGER_ID;


--seleccionar todos los empleados que no tienen jefe o no tienen subalternos

select E.LAST_NAME || ' ' || E.FIRST_NAME AS EMPLEADO,
J.LAST_NAME || ' ' || J.FIRST_NAME AS JEFE,
J.TITLE AS CARGO
FROM s_emp J FULL OUTER JOIN s_emp E
on J.ID = E.MANAGER_ID 
where J.ID is NULL or E.MANAGER_ID is null;