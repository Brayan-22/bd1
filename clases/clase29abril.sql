---seleccionar los nombres de los productos que nunca se han vendido

SELECT P.name producto
from s_product P
MINUS
SELECT P.name producto
from s_product P,s_item I
WHERE P.id = I.product_id;


--seleccionar los productos que tienen en inventario la region de norte america y que no tienen en la region de suramerica

select P.name Producto
from s_product P,s_inventory I,s_warehouse W,s_region R
where P.id = I.product_id AND
      W.id = I.warehouse_id AND
      R.id = W.region_id AND
      lower(R.name) like 'north%' AND
      I.AMOUNT_in_STOCK>0
MINUS
select P.name Producto
from s_product P,s_inventory I,s_warehouse W,s_region R
where P.id = I.product_id AND
      W.id = I.warehouse_id AND
      R.id = W.region_id AND
      lower(R.name) like 'south%' AND
      I.AMOUNT_in_STOCK>0;


--Seleccionar las personas relacionadas con la compañia indicando si son clientes o empleados

select E.first_name || ' ' || E.last_name Empleado,'Empleado' Relacion
from s_emp E
UNION
select C.name Persona, 'Cliente' Relacion
from s_customer C
ORDER by Relacion;


--seleccionar los productos que ha vendido la region de asia y los que ha vendido la region de suramerica
SELECT P.name Producto 
from s_product P,s_item I,s_ord O,s_customer C,s_region R
where P.id = I.product_id AND
      O.id = I.ord_id AND
      C.id = O.customer_id AND
      R.id = C.region_id AND
      lower(R.name) LIKE 'asia%'
INTERSECT
SELECT P.name Producto 
from s_product P,s_item I,s_ord O,s_customer C,s_region R
where P.id = I.product_id AND
      O.id = I.ord_id AND
      C.id = O.customer_id AND
      R.id = C.region_id AND
      lower(R.name) LIKE 'south%';

--manera correcta: 
select DISTINCT P.name Producto
from s_product P,s_inventory I,s_warehouse W,s_region R,s_item IT
where P.id = I.product_id AND
      W.id = I.warehouse_id AND
      R.id = W.region_id AND
      P.id = IT.product_id AND
      lower(R.name) LIKE 'asia%'
INTERSECT
select DISTINCT P.name Producto
from s_product P,s_inventory I,s_warehouse W,s_region R,s_item IT
where P.id = I.product_id AND
      W.id = I.warehouse_id AND
      R.id = W.region_id AND
      P.id = IT.product_id AND
      lower(R.name) LIKE 'south%';


--sql avanzado: 
select count(*) from s_emp;

select count(id) from s_emp;

SELECT COUNT(commission_pct) Numero
from s_emp;

select count(id) numero,sum(salary),min(salary), avg(salary)
from s_emp;

select count(id) numero,sum(payment),min(payment),avg(payment) from s_salary;


SELECT E.first_name || ' ' || E.last_name Empleado, S.datePayment ,S.payment Pago
from s_emp E,s_salary S
where E.id = S.id;

-- promedio de lo pagado por cada empleado
SELECT E.first_name || ' ' || E.last_name Empleado, AVG(S.payment) Promedio
from s_emp E,s_salary S
where E.id = S.id
GROUP BY E.first_name || ' ' || E.last_name;


-- seleccionar el numero de productos vendidos en cada orden

select I.ord_id Orden, sum(I.quantity_shipped) Nproductos
from s_item I
GROUP by I.ord_id;


-- seleccionar el numero de items vendidos en cada orden
select I.ord_id Orden, count(I.item_id) Nitems
from s_item I
GROUP by I.ord_id;

--seleccionar el numero de productos vendidos y items vendidos en cada orden
select I.ord_id Orden, count(I.item_id) Nitems,sum(I.quantity_shipped) Nproductos
from s_item I
GROUP by I.ord_id;


--seleccionar los empleados que hacen las ordenes y los que hacen el despacho

select DISTINCT F.first_name || ' ' || F.last_name Facturador,
                D.first_name || ' ' || D.last_name Despachador
from s_emp F,s_ord O,s_emp D
where F.id = O.sales_rep_id and
      D.id = O.stock_clerk;

--seleccionar los empleados con clientes

select E.first_name || ' ' || E.last_name Empleado
from s_emp E,s_customer C
where E.id = C.sales_rep_id;


-- seleccionar los empleados que de una orden hacen la orden, despachan la orden y consiguen el cliente
select DISTINCT O.id orden,F.first_name || ' ' || F.last_name Facturador,
                D.first_name || ' ' || D.last_name Despachador,
                R.first_name || ' ' || R.last_name Representante                
from s_emp F,s_ord O,s_emp D,s_customer C, s_emp R
where F.id = O.sales_rep_id and
      D.id = O.stock_clerk and
      R.id = C.sales_rep_id AND
      C.id = O.customer_id 
order by O.id;
