-- sum,min,max,avg,count


select count(id) ,count(commission_pct), sum(salary) , avg(salary)
from s_emp;


-- seleccionar el numero de pagos y el valor total pagado por concepto de salarios


select count(consec),sum(payment) total
from s_salary;

--seleccionar el numero de pagos y el valor total, por concepto de salariosm pagado a cada uno de los empleados

select id,count(consec),sum(payment) total
from s_salary  
group by id;


select E.first_name || ' ' || E.last_name Empleado, count(consec) Npagos,sum(S.payment) Total
from s_emp E, S_salary S
where E.id = S.id
GROUP by E.first_name,E.last_name; 

--Seleccionar por mes el valor pagado y promedio pagado por conceptos de salarios



select to_char(datepayment,'MM') Mes,count(consec) Npagos,sum(payment) Total
from s_salary
where to_char(datepayment,'MM') = '08'
GROUP by to_char(datepayment,'MM')
order by to_char(datepayment,'MM');

-- seleccionar por mes el valor pagado y promedio pagado por conceptos de salarios,
-- del total pagado superior a $60000

select to_char(datepayment,'MM') Mes,count(consec) Npagos,sum(payment) Total
from s_salary
having sum(payment) > 60000
GROUP by to_char(datepayment,'MM')
order by to_char(datepayment,'MM');

-- seleccionar los empleados que les han pagado en total (por concepto de salario)
-- mas de 10.000 dolares

select E.first_name || ' ' || E.last_name Empleado, count(consec) Npagos,sum(S.payment) Total
from s_emp E, S_salary S
where E.id = S.id
HAVING count(consec)>12
GROUP by E.first_name,E.last_name; 

-- seleccionar el valor en ventas por cada una de las regiones
select sum(o.total), r.name 
from s_ord o, s_emp e, s_dept d, s_region r 
where r.id = d.region_id 
and e.region_id = d.region_id 
and e.dept_id = d.id 
and sales_rep_id = e.id 
group by r.name;
-- seleccionar el valor total en ventas realizadas por region y departamento 

select sum(O.total) ventas,D.name Dpto, R.name region
from s_region R,s_dept D, s_emp E, S_ord O
where R.id = D.region_id AND
    D.id = E.dept_id and D.region_id = E.region_id AND
    E.id = O.sales_rep_id
    GROUP by R.name,D.name;

-- seleccionar los clientes de cada region que han hecho mas de 5 ordenes

select R.name regiom, C.name cliente,count(O.id) cuenta
from s_customer C, s_ord O,s_region R
where R.id = C.region_id AND
      C.id = O.customer_id
      GROUP by R.name,C.name;

-- seleccionar los productos que han vendido mas de 5 unidades

